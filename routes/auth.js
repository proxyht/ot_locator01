const express = require('express');
const route = express.Router();
const axios = require('axios');
const CONST = require('../constants');
const FUNC = require('../modules/customFunc');
const db = require('../modules/database');

route.get('/', (req, res) => {
    let params = req.query;

    if (params.hmac === undefined || params.shop === undefined || params.timestamp === undefined)
        res.status(403).send("Invalid parameters");

    let hmac = params.hmac; // I have to verify hmac later
    let shop = params.shop;
    let timestamp = params.timestamp;

    let shopifyApiKey = process.env.SHOPIFY_API_KEY;

    let scopes = 'read_content,write_content,read_script_tags,write_script_tags,write_locales,read_themes';

    let redirectURI = `${CONST._rootAppURI}/auth/callback`;
    let nonce = `${shop}_${timestamp}_${FUNC.randomStr(4)}`;
    let access_mode = 'offline'; // don't need this, but specified
    let shopifyhook = `https://${shop}/admin/oauth/authorize?client_id=${shopifyApiKey}&scope=${scopes}&redirect_uri=${redirectURI}&state=${nonce}&grant_options[]=${access_mode}`;

    console.log("FIRST HOOK RECEIVED AND PROCESSED. REDIRECTING TO " + shopifyhook);



    // redirect back to shopify to finish installing
    res.redirect(shopifyhook);
});

route.get('/callback', async (req, res) => {
    // the app is now installed at user store

    console.log("Verifying");

    let params = req.query;

    let code = params.code,
        hmac = params.hmac,
        shop = params.shop,
        state = params.state;

    let queryString = '';
    for (param in params) {
        if (param !== 'hmac')
            queryString += `${param}=${params[param]}&`;
    }

    // remove the last '&' character
    queryString = queryString.substr(0, queryString.length - 1);

    // if request is valid
    if (FUNC.verifynonce(state) && FUNC.verifyhmac(queryString, hmac) && FUNC.verifyhostname(shop)) {
        console.log("Verification ok");
        // check if the request is instalation (init) or normal request
        let checkInstallation = await db.checkUserExist(shop);
        console.log(checkInstallation);

        // get access token
        let requestToGetAccessToken = await axios.post(`https://${shop}/admin/oauth/access_token`, {
            client_id: process.env.SHOPIFY_API_KEY,
            client_secret: process.env.SHOPIFY_API_SECRET_KEY,
            code: code,
        })
        let accessToken = requestToGetAccessToken.data.access_token;

        // if user doesn't exist in the db
        if (!checkInstallation.exist) {
            let userJSON = {
                shop: shop,
                accessToken: accessToken,
                registerAt: FUNC.timeStampToDateTime(Date.now() / 1000),
            }

            // add webhooks
            FUNC.initWebHooks(accessToken);
            console.log(`webhooks added for ${shop}`);

            // add script tags
            FUNC.initScriptTags(accessToken);
            console.log(`script tags added for ${shop}`);

            let registerAction = await db.registerUser(userJSON);

            res.redirect(`https://${shop}/admin/apps`);

            // verify payment process
            // make a graphql request to shopify
            // let requestPayment = await FUNC.requestPayment(shop, accessToken);

            // if (requestPayment.appSubscriptionCreate !== undefined) {
            //     // if created payment request to shopify 
            //     let confirmationLink = requestPayment.appSubscriptionCreate.confirmationUrl;
            //     console.log(confirmationLink);
            //     db.updateConfirmationLink(shop);

            //     res.status(200).send(`
            //         <h2>Please click the button below to confirm your payment request!</h2>
            //         <a href="${confirmationLink}" target="blank">
            //             <button type="button" href="go" class="btn btn-primary btn-lg">Confirm</button>
            //         </a>
            //         <h2>Or click on this link: </h2>
            //         <a href="${confirmationLink}" target="blank">
            //             ${confirmationLink}
            //         </a>
            //         `);
            // } else {
            //     res.status(200).send("<h3>Error verifying your subscription. Please try again!</h3> <br> Error code:<br>");
            // }


        } else { // if user has or used to have the app in his/her store
            let lastTimeInstall = new Date((await db.getLastTimeInstall(shop)).data);
            let timeNow = new Date();
            let isOverdue = FUNC.isOverdue(lastTimeInstall, timeNow);
            let currentPlan = (await db.getCurrentPlan(shop)).data;
            console.log("dued:",isOverdue);
            
            if (checkInstallation.active === 'active') { // if user has the app in store
                // console.log((await db.getLastTimeInstall(shop)),lastTimeInstall,timeNow,currentPlan,isOverdue);
                if (currentPlan !== 'free' && isOverdue) { // if overdued and current plan is not free
                    console.log("changing plan");
                    
                    db.changePlan(shop, 'free');

                    // now redirect user to the upgrade page
                }

                // returning the app interface
                if (currentPlan !== 'free')
                    res.redirect(`/?shop=${shop}&token=${accessToken}&plan=${currentPlan}&isOverdue=${isOverdue}`);
                else
                    res.redirect(`/?shop=${shop}&token=${accessToken}&plan=${currentPlan}`);                    

            } else { // if user is reinstalling the app
                console.log("Reinstalling");
                
                db.activateUser(shop);
                db.updateAccessToken(shop,accessToken);

                if (isOverdue) {
                    await db.changePlan(shop, 'free');
                }

                // add webhooks
                FUNC.initWebHooks(accessToken);
                console.log(`webhooks added for ${shop}`);

                // add script tags
                FUNC.initScriptTags(accessToken);
                console.log(`script tags added for ${shop}`);
                res.redirect(`https://${shop}/admin/apps`)

                // verify payment process
                // make a graphql request to shopify
                // let requestPayment = await FUNC.requestPayment(shop, accessToken);

                // if (requestPayment.appSubscriptionCreate !== undefined) {
                    // let confirmationLink = requestPayment.appSubscriptionCreate.confirmationUrl;
                //     console.log(confirmationLink);
                //     db.updateConfirmationLink(shop, confirmationLink);

                //     res.status(200).send(`
                //     <h2>Please click the button below to confirm your payment request!</h2>
                //     <a href="${confirmationLink}" target="blank">
                //         <button type="button" href="go" class="btn btn-primary btn-lg">Confirm</button>
                //     </a>
                //     <h2>Or click on this link: </h2>
                //     <a href="${confirmationLink}" target="blank">
                //         ${confirmationLink}
                //     </a>
                //     `);
                // } else {
                //     res.status(200).send("<h3>Error verifying your subscription. Please try again!</h3> <br> Error code:<br>");
                // }
            }
        }
    } else {
        res.status(403).send("Verification error");
    }
});

// after the charge is confirmed, a request is fired in to this route
route.get('/chargeConfirm', async (req, res) => {
    console.log("CHARGE CONFIRMED ",req.query)
    // update the plan section in tbl_usersettings
    let shop = req.query.shop,
        token = req.query.token,
        plan = req.query.plan;
    
    // update plan field in db and update the installation date
    await db.changePlan(shop,plan);
    res.redirect(`https://${shop}/admin/apps/${CONST._appName}`)

    // res.redirect(`/?shop=${shop}&token=${token}`);
    
    // let registerAt = FUNC.timeStampToDateTime(Date.now() / 1000);
    // let userJson = {
    //     shop: shop,
    //     accessToken: accessToken,
    //     registerAt: registerAt,
    // }
    // db.activateUser(shop);
    // console.log("User activated! ", userJson);

    // // add webhooks
    // FUNC.initWebHooks(accessToken);
    // console.log(`webhooks added for ${shop}`);

    // // add script tags
    // FUNC.initScriptTags(accessToken);
    // console.log(`script tags added for ${shop}`);
    // res.redirect(`https://${shop}/admin/apps`)
});

module.exports = route