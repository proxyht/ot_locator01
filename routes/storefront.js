const express = require('express');
const router = express.Router();

const CONST = require('../constants');
const FUNC = require('../modules/customFunc');
const db = require('../modules/database');

// middleware, allow from all origin
router.use((req,res,next) => {
    res.header("Access-Control-Allow-Origin", "*");
    next();
})

router.get('/locations/all', async (req, res) => {
    let json = req.query;

    let {
        shop
    } = json;

    let returnJSON = await db.getAllLocations({
        shop: shop,
        queryFields: ['idlocations', 'shop', 'custom_address', 'store_name','address','lat','lng','priority','custom_province','custom_country','zip_code','phone','email','fax','website','tags','note','store_image','marker_image']        
    });

    res.status(200).send(returnJSON);
})

router.get('/settings', async (req, res) => {
    let json = req.query;
    try{
        let response = await db.getStorefrontSettings(json);    
        res.status(200).send(response)    
    } catch (err) {
        console.error(err);
        res.status(500).send(err);
    }
})

router.get('/layouts', async (req,res) => {
    let json = req.query;
    try{
        let response = await db.getStorefrontLayoutSettings(json);    
        res.status(200).send(response); 
    } catch (err) {
        console.error(err);
        res.status(500).send(err);
    }
})

module.exports = router;