const express = require('express');
const path = require('path');
const fs = require('fs');
const router = express.Router();

const CONST = require('../constants');
const FUNC = require('../modules/customFunc');
const db = require('../modules/database');

const multer = require('multer');
const upload = multer({
    dest: 'uploads/'
})

checkAuthExist = (obj) => {
    return (!!obj.token && !!obj.shop)
}

// middleware
// verify request with shop and token params
router.use(upload.single('importFile'), async (req, res, next) => {
    let {
        shop,
        token,
    } = (checkAuthExist(req.query)) ? req.query: req.body;
    let googleAPIKey = (req.query.googleApiKey !== undefined) ?
        req.query.googleAPIKey : req.body.googleAPIKey;

    let queryResult = await db.verifyUser(shop, token);

    if (queryResult.status) {
        req.googleAPIKey = googleAPIKey;
        req.shop = shop;

        next();
    } else {
        console.log("Fishy detected");
        res.status(403).send("Unauthorized");
    }
});


router.get('/', (req, res) => {
    console.log(`Hello world ${req.query.s}`);
    res.status(200).send(`Hello world ${req.query.s}`);
});

router.get('/locations/get', async (req, res) => {

    let json = req.query;
    let {
        shop,
        idlocations
    } = json;
    console.log("GETTING ", json);
    let returnJSON = await db.getLocationByID({
        idlocations: idlocations,
        shop: shop
    });
    res.status(200).send(returnJSON);
})

router.get('/locations/all', async (req, res) => {
    let json = req.query;

    let {
        shop
    } = json;

    let returnJSON = await db.getAllLocations({
        shop: shop,
        queryFields: ['idlocations', 'shop', 'custom_address', 'store_name', 'address', 'lat', 'lng', 'priority']
    });
    console.log("Returning ", returnJSON);

    res.status(200).send(returnJSON);
})

router.post('/locations/add', async (req, res) => {
    // add location
    let json = req.body;

    let queryResult = await db.addLocation(json);
    console.log(queryResult);
    res.status(200).send(queryResult);
})

router.put('/locations/modify', async (req, res) => {
    let json = req.body;

    let queryResult = await db.modifyLocation(json)
    res.status(200).send(queryResult);
})

router.delete('/locations/delete', async (req, res) => {
    let json = {
        shop: req.body.shop,
        locations: [req.body.location],
    }

    let queryResult = await db.deleteLocations(json);
    res.status(200).send(queryResult);
})

router.delete('/locations/deleteSelected', async (req, res) => {
    let json = {
        shop: req.body.shop,
        locations: req.body.locations,
    }

    let queryResult = await db.deleteLocations(json);
    res.status(200).send(queryResult);
})

router.delete('/locations/deleteAll', async (req, res) => {
    let json = req.body;

    let queryResult = await db.deleteAllLocations(json);
    res.status(200).send(queryResult);
})

router.get('/settings/get', async (req, res) => {
    let obj = await db.getAllSettings({
        shop: req.query.shop
    });

    // provide user with the settings
    let returnJSON = obj.status ? obj.data : null
    res.status(200).send(returnJSON);
})

router.post('/settings/change', async (req, res) => {
    let json = req.body;
    let queryResult = await db.changeSettings(json);

    res.status(200).send(queryResult);
})

router.get('/settings/layout/get', async (req, res) => {
    let obj = await db.getLayoutSettings({
        shop: req.query.shop
    });

    // provide user with the settings
    let returnJSON = obj.status ? obj.data : null
    res.status(200).send(returnJSON);
})
// POST
// /api/settings/layout/change

router.post('/settings/layout/change', async (req, res) => {
    let json = req.body;
    let queryResult = await db.changeLayoutSettings(json);

    res.status(200).send(queryResult);
});

router.post('/settings/language/add', async (req, res) => {
    let json = req.body;
    console.log("JSON", json);

    let queryResult = await db.addLanguage(json);
    res.status(200).send(queryResult);
})

router.get('/export', async (req, res) => {
    console.log("REQUESTING FOR ", req.shop);
    let locations = await FUNC.exportFromDatabaseToCSV(req.shop);
    res.status(200).send(locations);
})

router.post('/import', upload.single('importFile'), async (req, res) => {
    let absoluteFilePath = path.join(__dirname, `/../${req.file.path}`);

    // now file is uploaded temporarily in /uploads. 
    // We need to read and import with that content
    let importStatus = await FUNC.importToDatabaseFromCSV(absoluteFilePath, req.googleAPIKey, req.shop);

    // after importing, delete the file
    fs.unlink(absoluteFilePath, function () {
        // some callback. Leave blank for now (do nothing)
    });

    // return the import status
    res.status(200).send(importStatus);
})

router.get('/pricing/getCurrentPlan', async (req, res) => {
    let obj = await db.getCurrentPlan(req.query.shop);
    if (obj.status)
        res.status(200).send(obj.data);
    else
        res.status(200).send('free');
})

router.post('/pricing/upgrade', async (req, res) => {
    let json = req.body;
    let shop = json.shop,
        accessToken = json.token,
        plan = json.plan;

    console.log(req.body);
    // verify payment process
    // make a graphql request to shopify

    let requestPayment = await FUNC.requestPayment(shop, accessToken, plan);

    if (requestPayment.appSubscriptionCreate !== undefined) {
        let confirmationLink = requestPayment.appSubscriptionCreate.confirmationUrl;

        db.updateConfirmationLink(shop, confirmationLink);

        res.status(200).send({
            status: 1,
            data: confirmationLink // on client, the browser must redirect to this confirmationLink for the user to verify
        });
    } else {
        res.status(200).send({
            status: 0,
        });
    }
})

module.exports = router