// database module using normal mysql (without xdevapi)

const CONST = require('../constants');

const mysql = require('mysql');
const util = require('util');

let conn = mysql.createConnection({
    host: process.env.HOST,
    user: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_SCHEMA,
});

const query = util.promisify(conn.query).bind(conn);

conn.connect(async function (err) {
    if (err) console.log("Cannot connect to db. Err: " + err);
    else {
        // connected

        // exports.test = () => {
        //         return appdb
        //             .createCollection('test')
        //             .catch((err) => {
        //                 console.log(err);
        //                 return 0;
        //             })
        //     }

        exports.getLocationByID = async (json) => {
            console.log("Requesting for location", json);

            let {
                shop,
                idlocations
            } = json;
            let shopData = null;

            let sqlCommand = `SELECT store_name,priority,custom_address,custom_province,custom_country,zip_code,phone,email,fax,website,note,
                                        CONVERT(store_image USING utf8) as store_image,CONVERT(marker_image USING utf8) as  marker_image,tags
                                FROM locations
                                WHERE shop=${mysql.escape(shop)} AND idlocations=${mysql.escape(idlocations)}`;

            try {
                let sqlResult = await query(sqlCommand);
                shopData = sqlResult[0];
                console.log("DATA", shopData);

            } catch (e) {
                console.log(`Error fetching location ${idlocations}: ${e}`);
                return {
                    status: 0,
                }
            }
            return {
                status: 1,
                location: shopData
            }

        }

        exports.getAllLocations = async (json) => {
            // json contains shop, queryFields

            let shop = json.shop;
            let queryFields = json.queryFields;

            let sqlCommand = `SELECT ${queryFields.join(',')}
                                    FROM locations
                                    WHERE shop = "${shop}"`;
            console.log(sqlCommand);

            let locations;
            try {
                locations = await query(sqlCommand);
            } catch (e) {
                return {
                    status: 0,
                    error: e,
                }
            }
            console.log(locations);
            return {
                status: 1,
                locations: locations,
            }

        }

        exports.addLocation = async (json) => {
            // add Location to db
            let shop = json.shop;

            let {
                address,
                city,
                area,
                state,
                lat,
                lng,
                store_name,
                priority,
                custom_address,
                custom_province,
                custom_country,
                zip_code,
                phone,
                email,
                fax,
                website,
                note,
                store_image,
                marker_image,
                tags
            } = json.location;
            // console.log(`Adding location for shop '${shop}'`);
            let sqlCommand = `INSERT INTO locations
                    (shop, address, city, area, state, lat, lng, store_name, priority,
                        custom_address, custom_province, custom_country, zip_code, phone, email,
                        fax, website, note, store_image, marker_image, tags)
                                    VALUES (${mysql.escape(shop)},${mysql.escape(address)},${mysql.escape(city)},${mysql.escape(area)},${mysql.escape(state)},${mysql.escape(lat)},${mysql.escape(lng)},${mysql.escape(store_name)},${mysql.escape(priority)},${
                                        mysql.escape(custom_address)},${mysql.escape(custom_province)},${mysql.escape(custom_country)},${mysql.escape(zip_code)},${mysql.escape(phone)},${mysql.escape(email)},${
                                        mysql.escape(fax)},${mysql.escape(website)},${mysql.escape(note)},${mysql.escape(store_image)},${mysql.escape(marker_image)},${mysql.escape(tags)})
                                    `
            console.log(sqlCommand);

            try {
                let sqlResult = await query(sqlCommand);
            } catch (e) {
                console.log(`Error adding store for ${shop}: ${e}`);
                return {
                    status: 0,
                    error: err,
                }
            }
            return {
                status: 1,
            }

        }

        exports.modifyLocation = async (json) => {
            let shop = json.shop;
            let {
                idlocations,
                address,
                city,
                area,
                state,
                lat,
                lng,
                store_name,
                priority,
                custom_address,
                custom_country,
                custom_province,
                zip_code,
                phone,
                email,
                fax,
                website,
                note,
                store_image,
                marker_image,
                tags
            } = json.location;

            let sqlCommand = `UPDATE locations
                                    SET address=${mysql.escape(address)},
                                        city=${mysql.escape(city)},
                                        area=${mysql.escape(area)},
                                        state=${mysql.escape(state)},
                                        lat=${mysql.escape(lat)},
                                        lng=${mysql.escape(lng)},
                                        store_name=${mysql.escape(store_name)},
                                        priority=${mysql.escape(priority)},
                                        custom_address=${mysql.escape(custom_address)},
                                        custom_province=${mysql.escape(custom_province)},
                                        custom_country=${mysql.escape(custom_country)},
                                        zip_code=${mysql.escape(zip_code)},
                                        phone=${mysql.escape(phone)},
                                        email=${mysql.escape(email)},
                                        fax=${mysql.escape(fax)},
                                        website=${mysql.escape(website)},
                                        note=${mysql.escape(note)},
                                        store_image=${mysql.escape(store_image)},
                                        marker_image=${mysql.escape(marker_image)},
                                        tags=${mysql.escape(tags)}
                                    WHERE idlocations=${mysql.escape(idlocations)} AND shop=${mysql.escape(shop)}
                                    `;

            try {
                console.log(sqlCommand);

                let mysqlResult = await query(sqlCommand);
            } catch (e) {
                console.log(`Error updating location ${idlocations} for shop ${shop}: ${e}`);
                return {
                    status: 0,
                }
            }
            return {
                status: 1,
            }
        }

        exports.deleteLocations = async (json) => {
            // default request = {
            //     shop: shop_name,
            //     locations: [], // locations to delete
            // }
            let shop = json.shop;
            let locations = json.locations;
            let deleted = 0;
            console.log("LOCATIONS : ", locations);

            for (let index in locations) {
                let location = locations[index]
                let idlocations = location.idlocations;
                let sqlCommand = `DELETE FROM locations
                                        WHERE shop=${mysql.escape(shop)} 
                                        AND idlocations=${mysql.escape(idlocations)}`;
                console.log(sqlCommand);

                try {
                    let sqlResult = await query(sqlCommand);
                } catch (e) {
                    console.log(`Error deleting location: ${e}`)
                }
                deleted++;
            }
            return {
                status: deleted,
            }
        }

        exports.deleteAllLocations = async (json) => {
            let shop = json.shop;

            let sqlCommand = `DELETE FROM locations
                                WHERE shop=${mysql.escape(shop)}`;

            try {
                let sqlResult = await query(sqlCommand);
            } catch (e) {
                console.log(`Error deleting all locations for shop ${shop}: ${e}`);
                return {
                    status: 0,
                }
            }
            return {
                status: 1,
            }
        }

        // get wrapperClass, sectionHeader and googleAPIKey
        exports.getStorefrontSettings = async (json) => {

            let shop = json.shop,
                general = {};

            let sqlCommand = `SELECT general
                            FROM settings
                            WHERE shop=${mysql.escape(shop)}`
            try {
                let sqlResult = await query(sqlCommand);
                general = sqlResult[0]
            } catch (e) {
                console.log(`Error fetching layout settings: ${e}`);
                return {
                    status: 0
                }
            };
            return {
                status: 1,
                data: general,
            }
        }

        exports.getStorefrontLayoutSettings = async (json) => {
            let shop = json.shop,
                layouts = {};

            let sqlCommand = `SELECT layout
                                    FROM settings
                                    WHERE shop="${mysql.escape(shop)}"`
            try {
                let sqlResult = await query(sqlCommand);
                layouts = sqlResult[0]
            } catch (e) {
                console.log(`Error fetching layout settings: ${e}`);
                return {
                    status: 0
                }
            };
            return {
                status: 1,
                data: layouts,
            }

        }

        exports.getAllSettings = async (json) => {
            let shop = json.shop,
                settings = {};

            let sqlCommand = `SELECT * FROM settings
                                WHERE shop=${mysql.escape(shop)}`;

            try {
                settings = await query(sqlCommand);

            } catch (e) {
                console.log(`Error fetching settings: ${e}`);
                return {
                    status: 0
                }
            }
            console.log(settings[0].general);
            if (!settings.length) // if settings are not fetched
                return {
                    status: 0
                }
            else {
                return {
                    status: 1,
                    data: settings[0].general,
                }
            }
        }

        exports.getLayoutSettings = async (json) => {
            let shop = json.shop,
                layoutSettings = {};
            let sqlCommand = `SELECT layout
                                    FROM settings
                                    WHERE shop=${mysql.escape(shop)}`;
            try {
                let sqlResult = await query(sqlCommand);
                console.log(sqlResult);

                layoutSettings = sqlResult[0].layout;
            } catch (e) {
                console.log(`Error fetching layout settings: ${e}`);
                return {
                    status: 0,
                }
            }

            return {
                status: 1,
                data: layoutSettings,
            }
        }

        exports.changeSettings = async (json) => {
            let shop = json.shop,
                settings = {},
                updatedSettings = json.settings;
            console.log(updatedSettings);
            let sqlCommand = `UPDATE settings
                                    SET general=${mysql.escape(JSON.stringify(updatedSettings))}
                                    WHERE shop=${mysql.escape(shop)}`
            try {
                console.log(sqlCommand);

                let sqlResult = await query(sqlCommand);
                console.log(sqlResult);

            } catch (e) {
                console.log(`Error changing settings: ${e}`);
                return {
                    status: 0
                }
            }
            return {
                status: 1,
            }
        }

        exports.addLanguage = async (json) => {
            let shop = json.shop,
                lang = json.lang;

            let sqlSelectCommand = `SELECT general FROM settings
                                    WHERE shop=${mysql.escape(shop)}`;

            try {
                let sqlSelectResult = await query(sqlSelectCommand);
                let generalSettings = JSON.parse(sqlSelectResult[0].general);
                generalSettings.content = {
                    ...generalSettings.content,
                    [lang]: {
                        "lang": "English",
                        "sectionHeader": "Our stores",
                        "storeName": "Store name",
                        "customAddress": "Address",
                        "customProvince": "Province",
                        "customCountry": "Country",
                        "zipCode": "Zip Code",
                        "phone": "Phone",
                        "email": "Email",
                        "fax": "Fax",
                        "website": "Website",
                        "note": "Note",
                        "tags": "Tags"
                    }
                }
                let sqlUpdateCommand = `UPDATE settings
                                        SET general=${mysql.escape(JSON.stringify(generalSettings))}
                                        WHERE shop=${mysql.escape(shop)}
                                        `;
                try {
                    let sqlUpdateResult = await query(sqlUpdateCommand);
                    return {
                        status: 1
                    }
                } catch (e) {
                    console.log(`Error adding language of ${shop}: ${e}`);

                    return {
                        status: 0,
                    }
                }
            } catch (e) {
                console.log(`Error fetching general settings of ${shop}: ${e}`);
                return {
                    status: 0,
                }
            }
        }

        exports.changeLayoutSettings = async (json) => {
            let shop = json.shop,
                updatedLayoutSettings = JSON.stringify(json.layoutSettings);

            let sqlCommand = `UPDATE settings
                                SET layout=${mysql.escape(updatedLayoutSettings)}
                                WHERE shop=${mysql.escape(shop)}
                                `;
            console.log(sqlCommand);

            try {
                let sqlResult = await query(sqlCommand);

            } catch (e) {
                return {
                    status: 0
                }
            }
            return {
                status: 1,
            }

        }

        exports.checkUserExist = async (shopURL) => {
            let exist = 0,
                active = 0;
            let sqlCommand = `SELECT status,installed_date FROM tbl_usersettings WHERE store_name=${mysql.escape(shopURL)}`;
            try {
                let sqlResult = await query(sqlCommand);
                if (sqlResult.length) {
                    exist = 1;
                    active = sqlResult[0].status;
                }
            } catch (e) {
                console.log(`Error checking user existance: ${e}`);
                return {
                    exist: exist,
                    active: active,
                }
            }
            return {
                exist: exist,
                active: active,
            }


            // let exist = 0,
            //     active = 0;
            // let sqlCommand = `SELECT active FROM users WHERE shop = "${shopURL}"`;

            // return await session
            //     .sql(sqlCommand)
            //     .execute(result => {
            //         exist = result.length > 0;
            //         active = result[0] === 1; // result[0] is the 'active' value
            //     })
            //     .then((_) => {
            //         return {
            //             exist: exist,
            //             active: active
            //         };
            //     })
            //     .catch((err) => {
            //         if (err) throw err;
            //         return {
            //             exist: exist,
            //             active: active
            //         };
            //     });
        }

        // add user to db
        exports.registerUser = async (json) => {
            let shop = json.shop,
                accessToken = json.accessToken,
                registerAt = json.registerAt;

            let defaultSettings = JSON.stringify(CONST._defaultSettings);
            let defaultLayoutSettings = JSON.stringify(CONST._defaultLayoutSettings);

            defaultSettings.googleAPIWhitelistDomains = [
                `https://${shop}/*`,
                `${CONST._rootAppURI}/?shop=${shop}&token=${accessToken}`
            ];

            let sqlCommand1 = `INSERT INTO tbl_usersettings
                                (access_token,store_name,installed_date,app_id,
                                    status,confirmation_url,plan_name)
                                VALUES (${mysql.escape(accessToken)},${mysql.escape(shop)},
                                        ${mysql.escape(registerAt)},${mysql.escape(CONST._appID)},
                                        'active',NULL,'free')`;
            let sqlCommand2 = `INSERT INTO settings
                                (shop,general,layout)
                                VALUES (${mysql.escape(shop)},${mysql.escape(defaultSettings)},${mysql.escape(defaultLayoutSettings)})`
            try {
                console.log("REGISTERING");

                let sqlResult1 = await query(sqlCommand1);
                let sqlResult2 = await query(sqlCommand2)
            } catch (e) {
                console.log(`Error registering user: ${e}`);
                return {
                    status: 0,
                }
            }
            return {
                status: 1,
            }


        }

        exports.updateConfirmationLink = async (shop, confirmationURL) => {
            let sqlCommand = `UPDATE tbl_usersettings
                            SET confirmation_url=${mysql.escape(confirmationURL)}
                            WHERE store_name=${mysql.escape(shop)}
                            `;
            try {
                let sqlResult = await query(sqlCommand);
            } catch (e) {
                console.log(`Error updating confirmation link for shop ${shop}: ${e}`);
                return {
                    status: 0,
                }
            }
            return {
                status: 1,
            }
        }

        exports.activateUser = async (shop) => {
            let sqlCommand = `UPDATE tbl_usersettings
                            SET status="active"
                            WHERE store_name=${mysql.escape(shop)}
                            `;

            console.log(sqlCommand);

            try {
                let sqlResult = await query(sqlCommand);
            } catch (e) {
                console.log(`Error activating user: ${e}`);
                return {
                    status: 0
                }
            }
            return {
                status: 1,
            }
        }

        exports.verifyUser = async (shop, token) => {
            let verification = 0;
            let sqlCommand = `SELECT status from tbl_usersettings WHERE store_name=${mysql.escape(shop)} AND access_token=${mysql.escape(token)}`;
            console.log(sqlCommand);

            let rows;
            try {
                rows = await query(sqlCommand);
            } catch (e) {
                console.log(`Error verifying user ${shop}: ${e}`)
                return {
                    status: 0,
                }
            }

            verification = !!rows.length && rows[0].status === 'active';

            return {
                status: verification
            }
        }

        exports.deactivateUser = async (shop) => {
            let sqlCommand = `UPDATE tbl_usersettings
                            SET status="inactive"
                            WHERE store_name=${mysql.escape(shop)}`;
            try {
                let sqlResult = await query(sqlCommand);
            } catch (e) {
                console.log(`Error deactivating user ${shop}: ${e}`);
                return {
                    status: 0
                }
            }
            return {
                status: 1,
            }
        }

        exports.changePlan = async (shop, plan) => {
            let sqlCommand = `UPDATE tbl_usersettings
                            SET plan_name=${mysql.escape(plan)}
                            WHERE store_name=${mysql.escape(shop)}
                            `;
            try {
                console.log(`changing plan to ${plan}`)
                let sqlResult = await query(sqlCommand);
                return {
                    status: 1
                }
            } catch (e) {
                console.log(`Error changing plan for ${shop}: ${e}`);
                return {
                    status: 0
                }
            }
        }

        exports.updateAccessToken = async (shop, token) => {
            let sqlCommand = `UPDATE tbl_usersettings
                            SET access_token=${mysql.escape(token)}
                            WHERE store_name=${mysql.escape(shop)}
                            `;

            try {
                let sqlResult = await query(sqlCommand);
                return {
                    status: 1,
                }
            } catch (e) {
                console.log(`Error updating token for ${shop}: ${e}`)
                return {
                    status: 0,
                }
            }
        }

        exports.getLastTimeInstall = async (shop) => {
            let sqlCommand = `SELECT installed_date
                            FROM tbl_usersettings
                            WHERE store_name=${mysql.escape(shop)}`;
            try {
                let sqlResult = await query(sqlCommand);

                return {
                    status: 1,
                    data: sqlResult[0].installed_date
                }
            } catch (e) {
                console.log(`Error getting last time install for ${shop}: ${e}`);
                return {
                    status: 0,
                }
            }
        }

        exports.getCurrentPlan = async (shop) => {
            let sqlCommand = `SELECT plan_name
                            FROM tbl_usersettings
                            WHERE store_name=${mysql.escape(shop)}`;
            try {

                let sqlResult = await query(sqlCommand);

                return {
                    status: 1,
                    data: sqlResult[0].plan_name
                }
            } catch (e) {
                console.log(`Error getting current plan for ${shop}: ${e}`);
                return {
                    status: 0,
                }
            }
        }

        // // delete user from db, delete all locations related to that user
        // exports.deleteUser = async (json) => {
        //     let shop = json.shop;

        //     return appdb
        //         .getTable('users')
        //         .delete()
        //         .where("shop = :shop")
        //         .bind('shop', shop)
        //         .execute()
        //         .then(_ => {
        //             return appdb
        //                 .getTable('locations')
        //                 .delete()
        //                 .where("shop = :shop")
        //                 .bind("shop", shop)
        //                 .execute()
        //                 .then(_ => {
        //                     return appdb
        //                         .getTable('settings')
        //                         .delete()
        //                         .where("shop = :shop")
        //                         .bind("shop", shop)
        //                         .execute();
        //                 })
        //         })
        //         .catch((err) => {
        //             if (err) {
        //                 throw (err);
        //             }
        //         });
        // }


        //     exports.importFromCSV = (file) => {

        //     }
    };
});