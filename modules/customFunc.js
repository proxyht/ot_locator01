const Axios = require('axios');
const CONST = require('../constants.js');
const db = require('./database');
const crypto = require('crypto');

exports.isOverdue = (installDate,now) => {
    let thirtyDays = 1000*3600*24*30 // miliseconds
    // thirtyDays = 1;
    let timeDiff = now-installDate;
    return timeDiff >= thirtyDays;
}

exports.randomStr = (length) => {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

exports.verifynonce = (nonce) => {
    // don't verify this for now
    return 1;
}

exports.verifyhmac = (queryString, hmac) => {
    const hash = crypto
        .createHmac('sha256', process.env.SHOPIFY_API_SECRET_KEY)
        .update(queryString)
        .digest('hex');

    return hash === hmac;
}

exports.verifyhostname = (hostname) => {
    const regExp = `[0-9,a-z,A-Z,-]*.myshopify.com$`;
    return hostname.match(regExp) === null ? 0 : 1;
}

// request payment from merchant
exports.requestPayment = async (shop, accessToken, plan) => {
    const {
        GraphQLClient
    } = require('graphql-request');

    // subscriptionQuery contains graphql body
    const subscriptionQuery = `mutation {
        appSubscriptionCreate(
          name: "Subscription Plan"
          returnUrl: "${CONST._rootAppURI}/auth/chargeConfirm?shop=${shop}&token=${accessToken}&plan=${plan}"
          trialDays: ${CONST._trialDays}
          test: ${CONST._testMode}
          lineItems: [{
            plan: {
              appRecurringPricingDetails: {
                  price: { amount: ${CONST._recurringPrice[plan]}, currencyCode: USD }
              }
            }
          }]
        ) {
          userErrors {
            field
            message
          }
          confirmationUrl
          appSubscription {
            id
          }
        }
      }`

    const client = new GraphQLClient(
        `${CONST._shopAPIURI}/unstable/graphql.json`, {
            headers: {
                "X-Shopify-Access-Token": accessToken
            }
        }
    );

    let result =
        client
        .request(subscriptionQuery)
        .then(response => {
            return response
        })
        .catch(err => {
            return err;
        })

    return result;
}

// update store's languages into database
exports.retrieveAndUpdateLanguages = async (shop, accessToken) => {
    const langQuery = ``
}

exports.initWebHooks = (accessToken) => {
    Axios.post(`${CONST._shopAPIURI}/${CONST._apiVersion}/webhooks.json`, {
        "webhook": {
            "topic": "app/uninstalled",
            "address": `${CONST._rootAppURI}/uninstall`,
            "format": "json"
        }
    }, {
        headers: {
            "X-Shopify-Access-Token": accessToken
        }
    }).then((_) => {
        return 1;
    }).catch((err) => {
        if (err) throw (err);
    })
}

exports.initScriptTags = (accessToken) => {
    Axios.post(`${CONST._shopAPIURI}/${CONST._apiVersion}/script_tags.json`, {
        "script_tag": {
            "event": "onload",
            "src": `${CONST._rootAppURI}/ot_app.js`
        }
    }, {
        headers: {
            "X-Shopify-Access-Token": accessToken
        }
    }).then((_) => {
        return 1;
    }).catch((err) => {
        if (err) throw (err.response.data.errors);
    })
}

exports.timeStampToDateTime = (timestamp) => {
    let d = new Date(timestamp * 1000);
    let year = d.getFullYear();
    let month = "0" + d.getMonth();
    let day = "0" + d.getDay();
    let hours = "0" + d.getHours();
    let minutes = "0" + d.getMinutes();
    let seconds = "0" + d.getSeconds();
    registerAt = year + "-" + month.substr(-2) + "-" + (day.substr(-2)) + " " + (hours.substr(-2)) + ":" + (minutes.substr(-2)) + ":" + (seconds.substr(-2));
    return registerAt;
}

const csvFields = ['map_address', 'city', 'area', 'state', 'lat', 'lng', 'store_name',
    'priority', 'custom_address', 'custom_province', 'custom_country',
    'zip_code', 'phone', 'email', 'fax', 'website', 'note', 'store_image',
    'marker_image', 'tags'
];
const errList = {
    invalidColumnName: 'Invalid column name. Make sure the fields\' name are exactly as the sample file',
    invalidLocation: 'Cannot retrieve geolocation data from the map_address provided. You will need to specify the marker position manually on the Locations tab, or else the marker position will be incorrect',
    unknown: 'Server error. Make sure your connection is stable. Report to us if this error happends',
}

exports.exportFromDatabaseToCSV = async (shop) => {
    let locationsQuery = await db.getAllLocations({
        shop: shop,
        queryFields: ['address', 'lat', 'lng', 'store_name', 'priority', 'custom_address', 'custom_province', 'custom_country', 'zip_code', 'phone', 'email', 'fax', 'website', 'note', 'store_image', 'marker_image', 'tags']
    });
    if (!!locationsQuery.status) {
        let locations = locationsQuery.locations;
        for (let index in locations) {
            locations[index].map_address = locations[index].address;
            delete locations[index].address;
        }
        // console.log(locations);
        const {
            Parser
        } = require('json2csv');

        const fields = ['map_address', 'lat', 'lng', 'store_name', 'priority', 'custom_address', 'custom_province', 'custom_country', 'zip_code', 'phone', 'email', 'fax', 'website', 'note', 'store_image', 'marker_image', 'tags'];
        const opts = {
            fields
        };

        try {
            const parser = new Parser(opts);

            const csv = parser.parse(locations);
            // console.log(csv);
            return csv;
        } catch (err) {
            console.error(err);
            return false;
        }

    } else {
        console.log(`DB Error. Can't export ${shop} data `)
    }
}

exports.importToDatabaseFromCSV = async (fileURL, googleAPIKey, shop) => {
    // import csv
    const csv = require('csvtojson');

    let rawJsonObjects = await csv().fromFile(fileURL);

    let err_stacks = [];
    let successCount = 0;
    let defaultAddressObj = {
        address: null,
        area: null,
        state: null,
        lat: 0,
        lng: 0,
    }

    for (let index in rawJsonObjects) {
        let row = rawJsonObjects[index];
        // Format the row.
        // Now we need to retrieve the address components (city,state,lat,lng) 
        // from google maps api. 

        let formattedRow = row;

        // console.log(row);
        row.lat = Number(row.lat);
        row.lng = Number(row.lng);

        // if (row.lat === 0) delete row.lat;
        // if (row.lng === 0) delete row.lng;
        
        for (const key in row) {
            // if invalid key, skip
            if (!csvFields.includes(key)) {
                err_stacks.push({
                    type: 'critical',
                    data: `Error at line ${Number(index)+1}. ${errList.invalidColumnName}`
                })
                continue;
            } else if (key === 'map_address') {
                let addressObj;
                if (row.lat !== 0 && row.lng !== 0) {
                    // if lat lng is set, we don't need to use google api to update it anymore
                    // if lat lng is already set
                    addressObj = defaultAddressObj;
                    addressObj.address = row.map_address;
                    addressObj.lat = row.lat;
                    addressObj.lng = row.lng;
                } else {
                    // this row will be processed to get other infos (city,state,lat,lng)
                    try {
                        addressObj = await getAddressObjFromAddress(row.map_address, googleAPIKey);
                        console.log('adderssObj', addressObj);
                        
                        if (addressObj.error_message) {
                            
                            // if cannot retrieve lat,lng,state,area,city... from map_address
                            err_stacks.push({
                                type: 'warning',
                                data: `Warning at line ${Number(index)+1}: ${errList.invalidLocation} (Google: ${addressObj.error_message})`
                            });
                            addressObj = defaultAddressObj;
                            addressObj.address = "NO MAP LOCATION SPECIFIED. CLICK 'EDIT' AND ADJUST THE MARKER TO THE LOCATION POSITION.";

                        } else if (addressObj.status === 'ZERO_RESULTS'){
                            // if cannot retrieve lat,lng,state,area,city... from map_address
                            err_stacks.push({
                                type: 'warning',
                                data: `Warning at line ${Number(index)+1}: ${errList.invalidLocation} (Google: ${addressObj.status})`
                            });
                            addressObj = defaultAddressObj;
                            addressObj.address = "NO MAP LOCATION SPECIFIED. CLICK 'EDIT' AND ADJUST THE MARKER TO THE LOCATION POSITION."
                        }
                    } catch (err) {
                        err_stacks.push({
                            type: 'warning',
                            data: `Warning at line ${Number(index)+1}. ${errList.invalidLocation}`
                        });
                        addressObj = defaultAddressObj;
                        addressObj.address = "NO MAP LOCATION SPECIFIED. CLICK 'EDIT' AND ADJUST THE MARKER TO THE LOCATION POSITION."
                    }
                }                

                formattedRow = {
                    ...formattedRow,
                    ...addressObj
                }
                console.log(formattedRow);
                
            } else if (key === 'lat' || key === 'lng'){
                continue;
            } else {
                formattedRow[key] = row[key];
            }
        }

        delete formattedRow.map_address;
        formattedRow.priority = Number.isInteger(formattedRow.priority) ? formattedRow.priority : 1;

        console.log("formattedRow",formattedRow);

        let json = {
            shop: shop,
            location: formattedRow,
        }


        // now insert into database
        let addStatus = await db.addLocation(json);

        if (!!addStatus.status) {
            successCount++;
        } else {
            err_stacks.push({
                type: 'critical',
                data: `Error at line ${Number(index)+1}. ${addStatus.error}`,
            })
        }

    }

    return {
        successCount: successCount,
        errors: err_stacks,
    }
}

// fetch from google api. Input: address string -> output: address components (state, city, lat,lng,...)
getAddressObjFromAddress = async (mapAddress, googleAPIKey) => {

    let endpoint = encodeURI(`https://maps.googleapis.com/maps/api/geocode/json?&address=${mapAddress}&key=${googleAPIKey}`);

    let response = await Axios.get(endpoint);
    // console.log(response.data.results.length);

    if (response.data.results.length === 0) {
        console.log("ERRRRRRRRRR", response.data.error_message)
        return response.data;
    }

    let addressArray = response.data.results[0].address_components;
    let {
        lat,
        lng
    } = response.data.results[0].geometry.location;
console.log(JSON.stringify(response.data.results[0].geometry.location));

    if (lat === null || lng === null) {
        lat = 20;
        lng = 100;
    }

    let city = getCity(addressArray),
        area = getArea(addressArray),
        state = getState(addressArray);
    let returnObj = {
        address: mapAddress,
        city,
        area,
        state,
        lat,
        lng,
    }

    return returnObj
}

/**
    * Get the city and set the city input value to the one selected
    *
    * 
  @param addressArray
    * 
  @return {string}
    */
getCity = (addressArray) => {
    if (addressArray == null)
        return null
    let city = '';
    for (let i = 0; i < addressArray.length; i++) {
        if (addressArray[i].types[0] && 'administrative_area_level_2' === addressArray[i].types[0]) {
            city = addressArray[i].long_name;
            return city ? city : null;
        }
    }
    return city;
};
/**
  * Get the area and set the area input value to the one selected
  *
  * 
@param addressArray
  * 
@return {string}
  */
getArea = (addressArray) => {
    if (addressArray == null)
        return null
    let area = '';
    for (let i = 0; i < addressArray.length; i++) {
        if (addressArray[i].types[0]) {
            for (let j = 0; j < addressArray[i].types.length; j++) {
                if ('sublocality_level_1' === addressArray[i].types[j] || 'locality' === addressArray[i].types[j]) {
                    area = addressArray[i].long_name;
                    return area
                }
            }
        }
    }
    return area;
};
/**
  * Get the address and set the address input value to the one selected
  *
  * 
@param addressArray
  * 
@return {string}
  */
getState = (addressArray) => {
    if (addressArray == null)
        return null;

    let state = '';
    for (let i = 0; i < addressArray.length; i++) {
        for (let i = 0; i < addressArray.length; i++) {
            if (addressArray[i].types[0] && 'administrative_area_level_1' === addressArray[i].types[0]) {
                state = addressArray[i].long_name;
                return state;
            }
        }
    }
    return state
};