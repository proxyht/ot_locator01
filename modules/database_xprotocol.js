// database module using xdevapi

const CONST = require('../constants');

const mysqlx = require('@mysql/xdevapi');

let options = {
    host: process.env.HOST,
    port: process.env.DB_PORT,
    user: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    schema: process.env.DB_SCHEMA,
};

let conn = mysqlx.getSession(options);
conn.then((session) => {
    appdb = session.getSchema(options.schema)
    exports.test = () => {
        return appdb
            .createCollection('test')
            .catch((err) => {
                console.log(err);
                return 0;
            })
    }

    exports.getLocationByID = (json) => {
        console.log("Requesting for location", json);

        let {
            shop,
            idlocations
        } = json;
        let shopData = null;

        return appdb
            .getTable('locations')
            .select(['shop', 'address', 'city', 'area', 'state', 'lat', 'lng', 'store_name', 'priority', 'custom_address', 'custom_country',
                'zip_code', 'phone', 'email', 'fax', 'website', 'note', 'store_image', 'marker_image', 'tags'
            ])
            .where('shop = :shop and idlocations = :idlocations')
            .bind('shop', shop)
            .bind('idlocations', idlocations)
            .execute((row) => {
                shopData = {
                    store_name: row[7],
                    priority: row[8],
                    custom_address: row[9],
                    custom_country: row[10],
                    zip_code: row[11],
                    phone: row[12],
                    email: row[13],
                    fax: row[14],
                    website: row[15],
                    note: row[16],
                    store_image: row[17],
                    marker_image: row[18],
                    tags: row[19],
                }
            })
            .then((_) => {
                return {
                    status: 1,
                    location: shopData,
                }
            })
            .catch((err) => {
                if (err) {
                    console.log(`DB: error fetching locations for shop ${shop}`);
                    console.log(err);
                    return {
                        status: 0
                    }
                }
            })
    }

    exports.getAllLocations = (json) => {
        // json contains shop, queryFields

        let shop = json.shop;
        let queryFields = json.queryFields;
        let arr = [];

        return appdb
            .getTable('locations')
            .select(queryFields)
            .where('shop = :shop')
            .bind('shop', shop)
            .execute((row) => {
                let newRow = {};
                for (let index in queryFields) {
                    newRow[queryFields[index]] = row[index];
                }

                arr.push(newRow);
            })
            .then((_) => {
                return {
                    status: 1,
                    locations: arr,
                }
            })
            .catch((err) => {
                if (err) {
                    console.log(`DB: error fetching locations for shop ${shop}`);
                    console.log(err);
                    return {
                        status: 0
                    }
                }
            })
    }

    exports.addLocation = (json) => {
        // add Location to db
        let shop = json.shop;

        let {
            address,
            city,
            area,
            state,
            lat,
            lng,
            store_name,
            priority,
            custom_address,
            custom_province,
            custom_country,
            zip_code,
            phone,
            email,
            fax,
            website,
            note,
            store_image,
            marker_image,
            tags
        } = json.location;

        // console.log(`Adding location for shop '${shop}'`);

        return appdb
            .getTable('locations')
            .insert(['shop', 'address', 'city', 'area', 'state', 'lat', 'lng', 'store_name', 'priority',
                'custom_address', 'custom_province', 'custom_country', 'zip_code', 'phone', 'email',
                'fax', 'website', 'note', 'store_image', 'marker_image', 'tags'
            ])
            .values([shop, address, city, area, state, lat, lng, store_name, priority,
                custom_address, custom_province, custom_country, zip_code, phone, email,
                fax, website, note, store_image, marker_image, tags
            ])
            .execute()
            .then(() => {
                return {
                    status: 1
                };
            })
            .catch((err) => {
                console.log(`DB: error adding locations for shop ${shop}`);
                console.log(err);
                return {
                    status: 0,
                    error: err,
                }
            })
    }

    exports.modifyLocation = (json) => {
        let shop = json.shop;
        let {
            idlocations,
            address,
            city,
            area,
            state,
            lat,
            lng,
            store_name,
            priority,
            custom_address,
            custom_country,
            custom_province,
            zip_code,
            phone,
            email,
            fax,
            website,
            note,
            store_image,
            marker_image,
            tags
        } = json.location;

        return appdb
            .getTable('locations')
            .update()
            .set('address', address)
            .set('city', city)
            .set('area', area)
            .set('state', state)
            .set('lat', lat)
            .set('lng', lng)
            .set('store_name', store_name)
            .set('priority', priority)
            .set('custom_address', custom_address)
            .set('custom_province', custom_province)
            .set('custom_country', custom_country)
            .set('zip_code', zip_code)
            .set('phone', phone)
            .set('email', email)
            .set('fax', fax)
            .set('website', website)
            .set('note', note)
            .set('store_image', store_image)
            .set('marker_image', marker_image)
            .set('tags', tags)
            .where('idlocations=:id')
            .bind('id', idlocations)
            .execute()
            .then(_ => {
                return {
                    status: 1
                };
            })
            .catch(err => {
                console.log(`DB: error updating location ${idlocations} for shop ${shop}`)
                console.log(err);
                return {
                    status: 0
                }
            })
    }

    exports.deleteLocations = (json) => {
        // default request = {
        //     shop: shop_name,
        //     locations: [], // locations to delete
        // }
        let shop = json.shop;
        let locations = json.locations;

        for (let index in locations) {
            let location = locations[index]
            let idlocations = location.idlocations;

            return appdb
                .getTable('locations')
                .delete()
                .where("shop = :shop AND idlocations = :idlocations")
                .bind('shop', shop)
                .bind('idlocations', idlocations)
                .execute()
                .then(_ => {
                    return {
                        status: 1
                    }
                })
                .catch(err => {
                    console.log(`error deleting location ${idlocations} for shop ${shop}`)
                    console.log(err);
                    return {
                        status: 0
                    }
                })
        }
    }

    exports.deleteAllLocations = async (json) => {
        let shop = json.shop;

        return appdb
            .getTable('locations')
            .delete()
            .where("shop = :shop")
            .bind('shop', shop)
            .execute()
            .then(_ => {
                return {
                    status: 1
                }
            })
            .catch(err => {
                console.log(`error deleting all locations for shop ${shop}`)
                console.log(err);
                return {
                    status: 0
                }
            })
    }

    // get wrapperClass, sectionHeader and googleAPIKey
    exports.getStorefrontSettings = async (json) => {
        let shop = json.shop,
            settings = {};

        let getSettings = await appdb
            .getTable('settings')
            .select(['settings'])
            .where('shop = :shop')
            .bind('shop', shop)
            .execute((row) => {
                settings = row[0];
            })
            .then((_) => {
                return settings;
            })
            .catch((err) => {
                if (err) {
                    console.log(`DB: error getting storefront settings for shop ${shop}`);
                    console.log(err);
                    settings = null;
                    return 0;
                }
            })

        if (!settings)
            return {
                status: 0
            }
        else {
            console.log("SETTINGS ", settings);

            return {
                status: 1,
                data: {
                    content: settings['content'],
                    zoomLevel: settings['zoomLevel'],
                    defaultCenter: settings['defaultCenter'],
                    wrapperClassOrID: settings['wrapperClassOrID'],
                    calculateDistanceby: settings['calculateDistanceBy'],
                    googleAPIKey: settings['googleAPIKey'],
                }
            }
        }
    }

    exports.getStorefrontLayoutSettings = async (json) => {
        let shop = json.shop,
            layouts = {};

        let getSettings = await appdb
            .getTable('settings')
            .select(['layout_settings'])
            .where('shop = :shop')
            .bind('shop', shop)
            .execute((row) => {
                layouts = row[0];
            })
            .then((_) => {
                return layouts;
            })
            .catch((err) => {
                if (err) {
                    console.log(`DB: error getting storefront layouts for shop ${shop}`);
                    console.log(err);
                    layouts = null;
                    return 0;
                }
            })

        if (!layouts)
            return {
                status: 0
            }
        else {
            console.log("layouts ", layouts);

            return {
                status: 1,
                data: layouts,
            }
        }
    }

    exports.getAllSettings = async (json) => {
        let shop = json.shop,
            settings = {};

        let getSettings = await appdb
            .getTable('settings')
            .select(['settings'])
            .where('shop = :shop')
            .bind('shop', shop)
            .execute((row) => {
                settings = row[0];
            })
            .then((response) => {
                return settings;
            })
            .catch((err) => {
                if (err) {
                    console.log(`DB: error getting storefront settings for shop ${shop}`);
                    console.log(err);
                    settings = null;
                    return 0;
                }
            })

        if (settings === null)
            return {
                status: 0
            }
        else {
            return {
                status: 1,
                data: settings
            }
        }
    }

    exports.getLayoutSettings = async (json) => {
        let shop = json.shop,
            layoutSettings = {};

        let getSettings = await appdb
            .getTable('settings')
            .select(['layout_settings'])
            .where('shop = :shop')
            .bind('shop', shop)
            .execute((row) => {
                layoutSettings = row[0];
            })
            .then((response) => {
                return layoutSettings;
            })
            .catch((err) => {
                if (err) {
                    console.log(`DB: error getting storefront settings for shop ${shop}`);
                    console.log(err);
                    layoutSettings = null;
                    return 0;
                }
            })

        if (layoutSettings === null)
            return {
                status: 0
            }
        else {
            return {
                status: 1,
                data: layoutSettings
            }
        }
    }

    exports.changeSettings = async (json) => {
        let shop = json.shop,
            settings = {},
            updatedSettings = json.settings;

        let getSettings = await appdb
            .getTable('settings')
            .select(['shop', 'settings'])
            .where('shop = :shop')
            .bind('shop', shop)
            .execute((row) => {
                let newRow = row[1];
                settings = newRow;
            })
            .then((response) => {
                return settings;
            })
            .catch((err) => {
                if (err) {
                    console.log(`DB: error getting settings for update process for shop ${shop}`);
                    console.log(err);
                    settings = null;
                    return 0;
                }
            })

        if (settings === null)
            return {
                status: 0
            };

        let readOnly = ["googleAPIWhitelistDomains"];

        // change settings
        for (let index in updatedSettings) {
            // if the current settings is updateable
            if (!readOnly.includes(index)) {
                settings[index] = updatedSettings[index];
            }
        }
        return appdb
            .getTable('settings')
            .update()
            .set('settings', settings)
            .where('shop = :shop')
            .bind('shop', shop)
            .execute()
            .then(_ => {
                return {
                    status: 1
                };
            })
            .catch(err => {
                console.log(`DB: error updating settings for shop ${shop}`)
                console.log(err);
                return {
                    status: 0
                }
            })
    }

    exports.changeLayoutSettings = async (json) => {
        let shop = json.shop,
            layoutSettings = {},
            updatedLayoutSettings = json.layoutSettings;

        let getSettings = await appdb
            .getTable('settings')
            .select(['shop', 'layout_settings'])
            .where('shop = :shop')
            .bind('shop', shop)
            .execute((row) => {
                let newRow = row[1];
                layoutSettings = newRow;
            })
            .then((response) => {
                return layoutSettings;
            })
            .catch((err) => {
                if (err) {
                    console.log(`DB: error getting layout_settings for update process for shop ${shop}`);
                    console.log(err);
                    settings = null;
                    return 0;
                }
            })

        if (layoutSettings === null)
            return {
                status: 0
            };

        let readOnly = [];

        // change settings
        for (let index in updatedLayoutSettings) {
            // if the current settings is updateable
            if (!readOnly.includes(index)) {
                layoutSettings[index] = updatedLayoutSettings[index];
            }
        }
        return appdb
            .getTable('settings')
            .update()
            .set('layout_settings', layoutSettings)
            .where('shop = :shop')
            .bind('shop', shop)
            .execute()
            .then(_ => {
                return {
                    status: 1
                };
            })
            .catch(err => {
                console.log(`DB: error updating layout_settings for shop ${shop}`)
                console.log(err);
                return {
                    status: 0
                }
            })
    }

    exports.checkUserExist = async (shopURL) => {
        let exist = 0,
            active = 0;
        let sqlCommand = `SELECT active FROM users WHERE shop = "${shopURL}"`;

        return await session
            .sql(sqlCommand)
            .execute(result => {
                exist = result.length > 0;
                active = result[0] === 1; // result[0] is the 'active' value
            })
            .then((_) => {
                return {
                    exist: exist,
                    active: active
                };
            })
            .catch((err) => {
                if (err) throw err;
                return {
                    exist: exist,
                    active: active
                };
            });

    }

    // add user to db
    exports.registerUser = (json) => {
        let shop = json.shop,
            accessToken = json.accessToken,
            registerAt = json.registerAt;

        let defaultSettings = CONST._defaultSettings;
        let defaultLayoutSettings = CONST._defaultLayoutSettings;

        defaultSettings.googleAPIWhitelistDomains = [
            `https://${shop}/*`,
            `${CONST._rootAppURI}/?shop=${shop}&token=${accessToken}`
        ];


        return appdb
            .getTable('users')
            .insert(['shop', 'accessToken', 'registerAt'])
            .values([shop, accessToken, registerAt])
            .execute()
            .then((_) => {
                return appdb
                    .getTable('settings')
                    .insert(['shop', 'settings', 'layout_settings'])
                    .values([shop, defaultSettings, defaultLayoutSettings])
                    .execute()
                    .then((_) => {
                        return 1;
                    })
                    .catch((err) => {
                        if (err) throw (err);
                    });
            })
            .catch((err) => {
                if (err) throw (err);
            });
    }

    exports.verifyUser = async (shop, token) => {
        let verification = 0;
        let sqlComand = `SELECT idusers from users WHERE shop="${shop}" AND accessToken="${token}"`;
        return await session
            .sql(sqlComand)
            .execute(result => {
                verification = (result.length === 1)
            })
            .then(_ => {
                return {
                    status: verification
                }
            })
            .catch(err => {
                console.log(`DB: Error verifying shop ${shop}`);
                console.log(err);
                return {
                    status: 0
                }
            })
    }

    // delete user from db, delete all locations related to that user
    exports.deleteUser = async (json) => {
        let shop = json.shop;

        return appdb
            .getTable('users')
            .delete()
            .where("shop = :shop")
            .bind('shop', shop)
            .execute()
            .then(_ => {
                return appdb
                    .getTable('locations')
                    .delete()
                    .where("shop = :shop")
                    .bind("shop", shop)
                    .execute()
                    .then(_ => {
                        return appdb
                            .getTable('settings')
                            .delete()
                            .where("shop = :shop")
                            .bind("shop", shop)
                            .execute();
                    })
            })
            .catch((err) => {
                if (err) {
                    throw (err);
                }
            });
    }


    exports.importFromCSV = (file) => {

    }
}).catch(e => {
    console.log("ERROR IN DB CONNECTION: " + e);

})