exports._appName = "tung-first-app";
exports._apiVersion = "2019-04";
exports._shopURI = "https://tung-first-app-store.myshopify.com";
exports._shopAPIURI = "https://tung-first-app-store.myshopify.com/admin/api";
exports._rootAppURI = "https://d6e89a35.ngrok.io";
exports._recurringPrice = {
    free: 0,
    premium: 5.99 // $5.99 per month per user
}; 
exports._trialDays = 0;
exports._testMode = true; // modify this on production
exports._appID = 1;
exports._defaultSettings = {
    wrapperClassOrID: ".main-content",
    calculateDistanceby: "kilometres",
    googleAPIKey: "abc",
    googleAPIWhitelistDomains: [
        "domainA",
        "domainB"
    ],
    defaultCenter: null,
    zoomLevel: 14,
    content: {
        "en": {
            "lang": "English",
            "sectionHeader": "Our stores",
            "storeName": "Store name",
            "customAddress": "Address",
            "customProvince": "Province",
            "customCountry": "Country",
            "zipCode": "Zip Code",
            "phone":  "Phone",
            "email": "Email",
            "fax": "Fax",
            "website": "Website",
            "note": "Note",
            "tags": "Tags"
        }
    }
};
exports._defaultLayoutSettings = {
    "layout": "style-1",
    "paddingSlides": "0px",
    "locationListWidth": "35%",
    "mapWidth": "65%",
    "mapAndListHeight": "400px",
    "responsiveBreakpoint": "960px"
}