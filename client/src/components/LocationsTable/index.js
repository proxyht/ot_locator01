import React from 'react';
import Axios from 'axios';
import './style.sass';
import { DataTable, Card, Button, ButtonGroup, Loading, TextField, Pagination, Checkbox } from '@shopify/polaris';
import LocationDialog from '../LocationDialog';
import { connect } from 'react-redux'

import { _getLocationsFromLocalStorage, _updateAllLocationsToLocalStorage } from '../../helper/localStorage';

const mapStateToProps = state => {
  return {
    queryString: state.AppAction.queryString,
  }
}

class LocationsTable extends React.Component {

  constructor() {
    super();
    this.state = {
      dialogType: null,
      activeLocation: null,
      locations: [],
      formattedLocations: [], // this will change due to user's sorting or searching
      formattedLocationsStatic: [], // this will not change
      checkboxStatus: [],
      checkboxStatusAll: false,
      countSelected: 0,
      isLoading: true,
      searchText: '',
      page: 1,
      resultPerPage: 20,
    }
  }

  handleChangePage = (type) => {
    if (type === 'increase') {
      if (this.state.page * this.state.resultPerPage < this.state.formattedLocations.length)
        this.setState({ page: this.state.page + 1 })
    }
    else if (type === 'decrease') {
      this.setState({ page: Math.max(this.state.page - 1, 1) })
    }
    console.log(this.state.page, this.state.formattedLocations.length);

  }

  handleCloseDialog = () => {
    this.setState({
      dialogType: null,
      activeLocation: null,
    })
  }

  showLoading = () => {
    this.setState({
      isLoading: true,
    })
  }

  showUpdateDialog = (location) => {
    this.setState({ activeLocation: location, dialogType: 'update' })
  }

  showDeleteDialog = (location) => {
    this.setState({ activeLocation: location, dialogType: 'delete' })
  }

  showDeleteCheckedDialog = () => {
    this.setState({ activeLocations: 1, dialogType: 'deleteSelected' });
  }

  showDeleteAllDialog = () => {
    this.setState({ dialogType: 'deleteAll' });
  }

  sortLocations = (rows, index, direction) => {
    return [...rows].sort((rowA, rowB) => {
      const valueA = rowA[index];
      const valueB = rowB[index];

      let descendingReturnValue = 0; // the sorting condition , default = 0 means we assume 2 elements are equal
      if (valueB > valueA)
        descendingReturnValue = 1;
      else if (valueB < valueA)
        descendingReturnValue = -1;

      return direction === 'descending' ? descendingReturnValue : -descendingReturnValue;
    });
  };

  handleSort = (rows) => (index, direction) => {
    this.setState({ formattedLocations: this.sortLocations(rows, index, direction) });
  };


  handleSearchTextChange = (value) => {
    this.setState({
      searchText: value,
      page: 1
    })

    // if search text is empty, return the default data
    if (value === '') {
      this.setState({
        formattedLocations: this.state.formattedLocationsStatic
      });
    }
    else {
      let tmp = [];
      for (let formattedLocation of this.state.formattedLocationsStatic) {
        for (let key = 1; key < formattedLocation.length - 1; key++) {  // exclude the last column (buttons) and the first column (checkboxes)
          let fieldValue = formattedLocation[key];
          if (typeof (fieldValue) === 'number')
            fieldValue = fieldValue.toString();

          if (fieldValue.includes(value)) {
            tmp.push(formattedLocation);
            break;
          }
        }
      }

      this.setState({
        formattedLocations: tmp
      })
    }
  }

  handleChangeCheckbox = async (idlocations = null) => {
    // if idlocations is not passed into the function, we will update all checkbox


    if (!idlocations) {
      let countSelected = this.state.checkboxStatusAll ? 0 : this.state.formattedLocations.length;

      await this.setState(state => {
        const checkboxStatus = state.checkboxStatus.map((item, key) => {
          return !state.checkboxStatusAll;
        });
        const formattedLocations = state.formattedLocations.map((item, key) => {

          let location = state.locations[key];
          item[0] =
            <Checkbox
              checked={!state.checkboxStatusAll}
              onChange={() => this.handleChangeCheckbox(location.idlocations)}
            />;
          return item

        });

        return {
          checkboxStatus,
          formattedLocations,
          checkboxStatusAll: !state.checkboxStatusAll,
          countSelected: countSelected,
        }
      })
    }
    else {
      let selectedToAdd = 0;

      await this.setState(state => {
        let isUncheck = false;

        const checkboxStatus = state.checkboxStatus.map((item, key) => {
          if (key === idlocations) {
            if (item) { // if a checkbox is being unchecked, uncheck the select all box
              isUncheck = true;
            }
            return !item
          } else {
            return item
          }
        });
        const formattedLocations = state.formattedLocations.map((item, key) => {
          let location = this.state.locations[key];

          if (location.idlocations === idlocations) {
            selectedToAdd += !this.state.checkboxStatus[location.idlocations] ? 1 : -1;

            item[0] =
              <Checkbox
                checked={!this.state.checkboxStatus[location.idlocations]}
                onChange={() => this.handleChangeCheckbox(location.idlocations)}
              />;
          }
          return item

        });
        let checkboxStatusAll = state.checkboxStatusAll;
        if (isUncheck)
          checkboxStatusAll = false;
        return {
          checkboxStatus,
          formattedLocations,
          checkboxStatusAll: checkboxStatusAll,
          countSelected: state.countSelected + selectedToAdd,
        }
      })
    };
    console.log(this.state.countSelected);
  }

  async componentDidMount() {
    let locations = _getLocationsFromLocalStorage();

    if (!locations) {
      // if cannot get locations from localstorage
      console.log("FETCHING LOCATIONS");
      let response = await Axios.get(`/api/locations/all${this.props.queryString}`);
      locations = response.data.locations;
      console.log(locations);

      if (locations && locations.length > 0)
        _updateAllLocationsToLocalStorage(locations);
    }

    if (locations) {
      // if successfully retrieve locations
      console.log("locatoins", locations);

      let formattedLocations = [];
      let checkboxStatus = [];
      for (let key in locations) {
        checkboxStatus[locations[key].idlocations] = false;
      }
      await this.setState({
        checkboxStatus: checkboxStatus
      });
      console.error(this.state.checkboxStatus)

      // format location so we can put it into the polaris datatable
      for (let key in locations) {
        let location = locations[key];

        console.error(this.state.checkboxStatus[location.idlocations])

        let formattedLocation = [
          <Checkbox
            checked={this.state.checkboxStatus[location.idlocations]}
            onChange={() => this.handleChangeCheckbox(location.idlocations)}
          />,
          (location.store_name !== null ? location.store_name : ""),
          (location.priority !== null ? location.priority : ""),
          (location.custom_address !== null ? location.custom_address : ""),
          (location.address !== null ? location.address : ""),
          <ButtonGroup>
            <Button onClick={() => this.showUpdateDialog(location)}>Edit</Button>
            <Button destructive onClick={() => this.showDeleteDialog(location)}>Delete</Button>
          </ButtonGroup>
        ];
        formattedLocations.push(formattedLocation);
      }
      this.setState({
        locations: locations,
        formattedLocations: formattedLocations,
        formattedLocationsStatic: formattedLocations,
        isLoading: false,
      });
    }
  }

  render() {
    return (
      <>

        {this.state.dialogType !== null &&
          <LocationDialog
            dialogType={this.state.dialogType}
            handleCloseDialog={this.handleCloseDialog}
            location={this.state.activeLocation}
            checkboxStatus={this.state.checkboxStatus}
            showLoading={this.showLoading}
          />
        }

        <>

          {this.state.isLoading &&
            <Loading />
          }

          <Card>
            <div className="tableHeaderContainer">
              <div className="addLocationBtn">
                <Button
                  onClick={() => this.setState({ dialogType: 'create', activeLocation: { lat: 40.7127753, lng: -74.0059728 } })}>Add a new location
                </Button>
                <span> </span>
                {this.state.countSelected > 0 &&
                  <Button
                    onClick={this.state.countSelected === this.state.formattedLocations.length ? this.showDeleteAllDialog : this.showDeleteCheckedDialog}
                    destructive
                  >
                    Delete {this.state.countSelected === this.state.formattedLocations.length ? "all" : this.state.countSelected} locations
                </Button>
                }
              </div>
              <div className="searchBox">
                <TextField
                  label="Search"
                  value={this.state.searchText}
                  onChange={this.handleSearchTextChange}
                />
              </div>
            </div>
            <Pagination
              hasPrevious
              onPrevious={() => {
                this.handleChangePage('decrease');
              }}
              hasNext
              onNext={() => {
                this.handleChangePage('increase');
              }}
            />
            <DataTable
              columnContentTypes={[
                'text',
                'text',
                'text',
                'text',
                'text',
                'text',
              ]}
              headings={[
                <Checkbox
                  checked={this.state.checkboxStatusAll}
                  onChange={() => this.handleChangeCheckbox()}
                />,
                'Shop name',
                'Priority',
                'Custom address',
                'Map address',
                'Actions',
              ]}
              rows={this.state.formattedLocations.slice((this.state.page - 1) * this.state.resultPerPage, this.state.page * this.state.resultPerPage)}
              sortable={[false, true, true, true, true, false]}
              defaultSortDirection="descending"
              // initialSortColumnIndex={4}
              onSort={this.handleSort(this.state.formattedLocations)}
              footerContent={`Showing ${(this.state.page - 1) * this.state.resultPerPage + 1} - ${Math.min(this.state.page * this.state.resultPerPage, this.state.formattedLocations.length)} of ${this.state.formattedLocations.length} results`}

            />
            <Pagination
              hasPrevious
              onPrevious={() => {
                this.handleChangePage('decrease');
              }}
              hasNext
              onNext={() => {
                this.handleChangePage('increase');
              }}
            /><br />

            <div className="addLocationBtn">
              <Button
                onClick={() => this.setState({ dialogType: 'create', activeLocation: { lat: 40.7127753, lng: -74.0059728 } })}>Add a new location
              </Button>
              <span> </span>
              {this.state.countSelected > 0 &&
                <Button
                  onClick={this.state.countSelected === this.state.formattedLocations.length ? this.showDeleteAllDialog : this.showDeleteCheckedDialog}
                  destructive
                >
                  Delete {this.state.countSelected === this.state.formattedLocations.length ? "all" : this.state.countSelected} locations
                </Button>
              }
            </div>
          </Card>
        </>
      </>
    )
  }
}
export default connect(mapStateToProps, null)(LocationsTable);