exports._getLocationsFromLocalStorage = () => {
    return JSON.parse(localStorage.getItem('locations'));
}

exports._updateAllLocationsToLocalStorage = locations => {
    // console.log("L ",locations)
    localStorage.setItem('locations',JSON.stringify(locations))
}

exports._addLocationToLocalStorage = (location) => {
    let locations = localStorage.getItem('locations') ? 
                JSON.parse(localStorage.getItem('locations')) : [];
    locations.push(location);
    exports._updateAllLocationsToLocalStorage(locations);
}

exports._updateLocationToLocalStorage = (location) => {
    return 1;
}

exports._deleteLocationFromLocalStorage = (locationToDelete) => {
    let locations = exports._getLocationsFromLocalStorage();
    console.log(locations, locationToDelete);
    
    for (let index in locations){
        let location = locations[index]
        if (location.idlocations === locationToDelete.idlocations){
            console.log("erase");
            locations.splice(index,1);
            break;
        }
    }
    exports._updateAllLocationsToLocalStorage(locations)
}

exports._deleteAllLocationsFromLocalStorage = () => {
    localStorage.removeItem('locations')
}
