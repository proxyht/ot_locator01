// working with server api

exports._helper_api_setAuth = (queryString) => {
    queryString = queryString.substr(1);
   
    let splittedParams = queryString.split('&');

    let paramsObj = {};
    for (let index in splittedParams) {
        let param = splittedParams[index].split('=');
        paramsObj[param[0]] = param[1]
    }
    return paramsObj;
}

exports._helper_api_makeParams = (queryString,currentLocation) => {
    let paramsObj = this._helper_api_setAuth(queryString);

    paramsObj['location'] = currentLocation
    return paramsObj
}
