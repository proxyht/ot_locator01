import React, { Component } from 'react';
import './style.sass';
import Axios from 'axios';
import { connect } from 'react-redux';
import { Button, Heading, Card, Loading, Banner, Spinner } from '@shopify/polaris';
import { _helper_api_setAuth, } from '../../helper/api';
import { _deleteAllLocationsFromLocalStorage } from '../../helper/localStorage';

const mapStateToProps = state => {
  return {
    queryString: state.AppAction.queryString,
    googleAPIKey: state.GoogleAction.googleAPIKey,
  };
};

function mapDispatchToProps(dispatch) {
  return {
  };
}

class ImportExport extends Component {
  constructor(props) {
    super(props);

    console.log(this.props);
    this.state = {
      importFile: null,
      importing: false,
      importResult: {
        successCount: 0,
        errors: [],
      }
    }
  }

  exportStoreData = async () => {
    let endpoint = `/api/export${this.props.queryString}`;
    const FileDownload = require('js-file-download');

    let response = await Axios.get(endpoint);
    FileDownload(response.data, 'locations.csv');  
  }

  importStoreData = async () => {
    this.setState({
      importing: true,
    });

    if (this.state.importFile === null)
      alert("Please select a .csv file")
    else {
      // do the import action

      let paramsObj = _helper_api_setAuth(this.props.queryString);
      paramsObj['importFile'] = this.state.importFile;

      const formConfig = {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      }

      // due to the content-type: 'multipart/form-data', 
      // we need to create a formData object in order to capture all text and file data
      let formData = new FormData();
      formData.append('googleAPIKey', this.props.googleAPIKey);

      for (let index in paramsObj) {
        formData.append(index, paramsObj[index]);
      }

      let uploadStatus = await Axios.post('/api/import', formData, formConfig);
      this.setState({
        importResult: uploadStatus.data
      });
      _deleteAllLocationsFromLocalStorage();
    }

    this.setState({
      importing: false,
    })
  }

  handleChangeImportFile = (e) => {
    console.log(e.target.files[0])
    this.setState({
      importFile: e.target.files[0],
    })
  }

  render() {
console.log(process.env.PUBLIC_URL);

    return (
      <>
        {this.state.importing &&
          <>
            <Loading />
            <Spinner size="large" color="teal" />
            <p>Importing</p>
            <p>The importing process may last longer if you have a large number of stores. Do not turn off this page </p>
          </>
        }
        {!this.state.importing && this.state.importResult.successCount > 0 &&
          <Banner title={this.state.importResult.successCount + " rows imported"} status='success' >
            <p>{this.state.importResult.successCount} rows imported!</p>
          </Banner>
        }

        {!this.state.importing
          &&
          this.state.importResult.errors.length > 0
          &&
          this.state.importResult.errors.map((error, key) => {
            return (
              <Banner key={key} title={error.data} status={error.type} >
                <p></p>
              </Banner>
            )
          })

        }

        <Card>
          <div className="importExportWrapper">
            <Heading>
              Import & Export
            </Heading>
            <input
              type="file"
              name="importFile"
              onChange={this.handleChangeImportFile}
            />
            <Button
              primary
              onClick={this.importStoreData}
            >
              Import
              </Button>
              <br/>
              <br/>
              {/* <Link url="/locations_example.csv">Download example file </Link> */}
              <Button
                primary
                onClick={this.exportStoreData}
              >
                Download current store data (.csv)
              </Button>
          </div>
        </Card>

        {/* <Card>
          <div className="importExportWrapper">
            <Heading>
              Export
            </Heading>
            
            <div className="importExportOption">
              <Button
                primary
                onClick={this.exportStoreData}
              >
                Export all stores (.csv)
              </Button>
            </div>

          </div>
        </Card> */}

      </>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ImportExport);