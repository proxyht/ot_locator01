import React, { Component } from 'react';
import LocationsTable from '../../components/LocationsTable';
// import { Page } from '@shopify/polaris';
import { connect } from 'react-redux';


const mapStateToProps = state => {
  return {
    googleAPIKey: state.GoogleAction.googleAPIKey,
  };
};

function mapDispatchToProps(dispatch) {
  return {
   
  };
}

class IndexPage extends Component {

  render() {
    return (
      // Important! Always set the container height explicitly   
      <>
        {(this.props && this.props.googleAPIKey) &&
          <LocationsTable />
        }
        {(this.props.googleAPIKey===undefined || this.props.googleAPIKey==="") &&
          <h1>Please provide a google api key in the settings panel</h1>
        }

        
      </>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(IndexPage);