import React, { Component } from 'react';
import './style.sass';
import Axios from 'axios';
import Map from '../../components/Map';
import { connect } from 'react-redux';
import { updateGoogleAPIKey, updateQueryString } from '../../actions';
import { Card, TextField, Link, Loading, Banner, Checkbox, Select, Button, Tooltip, Collapsible, TextContainer } from '@shopify/polaris';
import { _helper_api_makeParams, _helper_api_setAuth } from '../../helper/api';

const languages = [
  { key: 'af', value: 'af', label: 'Afghanistan' },
  { key: 'ax', value: 'ax', label: 'Aland Islands' },
  { key: 'al', value: 'al', label: 'Albania' },
  { key: 'dz', value: 'dz', label: 'Algeria' },
  { key: 'as', value: 'as', label: 'American Samoa' },
  { key: 'ad', value: 'ad', label: 'Andorra' },
  { key: 'ao', value: 'ao', label: 'Angola' },
  { key: 'ai', value: 'ai', label: 'Anguilla' },
  { key: 'ag', value: 'ag', label: 'Antigua' },
  { key: 'ar', value: 'ar', label: 'Argentina' },
  { key: 'am', value: 'am', label: 'Armenia' },
  { key: 'aw', value: 'aw', label: 'Aruba' },
  { key: 'au', value: 'au', label: 'Australia' },
  { key: 'at', value: 'at', label: 'Austria' },
  { key: 'az', value: 'az', label: 'Azerbaijan' },
  { key: 'bs', value: 'bs', label: 'Bahamas' },
  { key: 'bh', value: 'bh', label: 'Bahrain' },
  { key: 'bd', value: 'bd', label: 'Bangladesh' },
  { key: 'bb', value: 'bb', label: 'Barbados' },
  { key: 'by', value: 'by', label: 'Belarus' },
  { key: 'be', value: 'be', label: 'Belgium' },
  { key: 'bz', value: 'bz', label: 'Belize' },
  { key: 'bj', value: 'bj', label: 'Benin' },
]

const mapStateToProps = state => {
  return {
    googleAPIKey: state.GoogleAction.googleAPIKey,
    queryString: state.AppAction.queryString,
    currentLocation: state.AppAction.currentLocation,
  };
};

function mapDispatchToProps(dispatch) {
  return {
    updateGoogleAPIKey: apiKey => dispatch(updateGoogleAPIKey(apiKey)),
    updateQueryString: queryString => dispatch(updateQueryString(queryString))
  };
}

class Settings extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dialogType: this.props.dialogType,
      shouldBannerDisplay: false,
      bannerTitle: 'No changes have been made',
      bannerMsg: 'Remember to save your changes',
      bannerStatus: 'warning',
      settings: {
        wrapperClass: "",
        calculateDistanceby: "",
        googleAPIKey: "",
        googleAPIWhitelistDomains: [
          "domainA",
          "domainB"
        ],
        defaultCenter: null,
        zoomLevel: null,
        languageOptions: ['en', 'vi'],
        content: {

        }
      },
      currentLanguage: 'en',
      languageToAdd: "af",
      isLoading: true,
      copyText: {},
      addingLanguage: false,
    }
  }

  saveAllChanges = (e) => {
    e.preventDefault();

    let paramsObj = _helper_api_makeParams(this.props.queryString, this.props.currentLocation);
    paramsObj['settings'] = this.state.settings;
    console.log(paramsObj);

    // fire request to server
    Axios
      .post('/api/settings/change', paramsObj)
      .then((response) => {
        if (response.data.status) {
          this.setState({
            bannerTitle: "Settings updated",
            bannerStatus: 'success',
            bannerMsg: "All changes has been saved"
          });
          // setTimeout(() => window.location.reload(),500);
        }
        else {
          this.setState({
            bannerTitle: "Server error",
            bannerStatus: 'critical',
            bannerMsg: "No changes have been made"
          });
        }
      })
  }

  handleFormInputChange = (key, val) => {

    this.setState({
      shouldBannerDisplay: true,
      bannerTitle: 'No changes have been made',
      bannerMsg: 'Remember to save your changes',
      bannerStatus: 'warning',
      settings: {
        ...this.state.settings,
        [key]: val
      }
    })
  }
  async componentDidMount() {

    // get settings data from server
    let response = await
      Axios.get(`/api/settings/get${this.props.queryString}`)
        .catch((error) => {
          console.log("ERROR fetching settings");
          console.error(error);
          return null
        });
    if (response !== null) {
      this.setState({
        settings: response.data,
        isLoading: false,
      })
    }
  }

  handleLanguageSettingsChange = (lang) => {
    this.setState({
      // shouldBannerDisplay: true,
      currentLanguage: lang,
      addingLanguage: false,
    })
  }

  handleLanguageContentChange = (key, val) => {
    this.setState({
      shouldBannerDisplay: true,
      settings: {
        ...this.state.settings,
        content: {
          ...this.state.settings.content,
          [this.state.currentLanguage]: {
            ...this.state.settings.content[this.state.currentLanguage],
            [key]: val
          }
        }
      }
    })
  }

  handleCopyContent = (id) => {
    let inputID = "domain" + id;
    let copyText = document.getElementById(inputID);
    copyText.select();
    copyText.setSelectionRange(0, 99999); /*For mobile devices*/

    /* Copy the text inside the text field */
    document.execCommand(`copy`);

    /* Alert the copied text */
    this.setState({
      copyText: {
        ...this.state.copyText,
        [id]: 'Copied'
      }
    });
    setTimeout(() => {
      this.setState({
        copyText: {
          ...this.state.copyText,
          [id]: 'Copy'
        },
      })
    }, 4000)
    // alert("Copied the text: " + copyText.value);
  }

  handleLanguageAddToggleClick = () => {
    this.setState({
      addingLanguage: !this.state.addingLanguage,
    })
  }

  handleAddNewLanguage = async () => {
    console.log("Adding ",this.state.languageToAdd);

    let paramsObj = _helper_api_setAuth(this.props.queryString);
    paramsObj['lang'] = this.state.languageToAdd;

    let response = await Axios.post('/api/settings/language/add',paramsObj);
    console.log(response);
    if (response.data.status){
      // if successfully added language
      alert("Language added");
      window.location.reload();
    } else{
      alert("Server error. Please check your network connection and try again later")
    }
  }

  render() {
    let langList = [];
    for (let lang in this.state.settings.content) {
      langList.push({
        label: lang,
        value: lang
      })
    }
    // console.log("H", this.state.settings.content[this.state.currentLanguage].sectionHeader);

    return (
      <>
        {this.state.isLoading &&
          <Loading />
        }
        {!this.state.isLoading &&
          <>
            {this.state.shouldBannerDisplay &&
              <Banner title={this.state.bannerTitle} status={this.state.bannerStatus} >
                <p>{this.state.bannerMsg}</p>
              </Banner>
            }
            <Card>
              <div className="settingsWrapper">
                <Button
                  primary
                  onClick={this.saveAllChanges}
                >
                  Save all changes
                </Button>
              </div>
            </Card>

            <div className="divSectionContainer">
              <div className="divSettings">
                <Card
                  title="Set up your Google Maps API Key"
                >
                  <div className="inputContainer">
                    <TextField
                      name="googleAPIKey"
                      label="Google Maps API Key:"
                      value={this.state.settings.googleAPIKey}
                      onChange={val => this.handleFormInputChange("googleAPIKey", val)}
                      maxLength={100}
                    />
                    <Link url="#">See how to get google api key and set it up</Link>
                    <br />or<br />
                    <Link url="#">Watch a tutorial video</Link>
                    {this.state.settings.googleAPIWhitelistDomains && this.state.settings.googleAPIWhitelistDomains.map((domain, key) => {
                      return (
                        <div key={key}>
                          <TextField
                            id={"domain" + key}
                            readOnly
                            label={`Whitelist domain ${key + 1}: `}
                            value={domain}
                            suffix={
                              <Tooltip content={"Copy"} preferredPosition="above">
                                <Button plain onClick={() => this.handleCopyContent(key)}>{(key in this.state.copyText) ? this.state.copyText[key] : "Copy"}</Button>
                              </Tooltip>
                            }
                          >
                          </TextField>
                          {/* <input id={"domain"+key} type="hidden" value={domain}/> */}

                        </div>
                      )
                    })}
                  </div>
                </Card>
              </div>
            </div>

            <div className="divSectionContainer">
              <div className="divSettings halfWidthDiv">
                <Card
                  title="General"
                >
                  <div className="inputContainer">
                    <TextField
                      name="wrapperClassOrID"
                      label="Wrapper class or id:"
                      value={this.state.settings.wrapperClassOrID}
                      onChange={val => this.handleFormInputChange("wrapperClassOrID", val)}
                      maxLength={100}
                    />
                    <div className="selectContainer">
                      <Select
                        name="calculateDistanceBy"
                        label="Calculate distance by:"
                        value={this.state.settings.calculateDistanceby}
                        onChange={val => this.handleFormInputChange("calculateDistanceby", val)}
                        options={[
                          { label: 'km', value: 'km' },
                          { label: 'miles', value: 'miles' },
                        ]}
                      />
                    </div>
                  </div>
                </Card>
              </div>

              <div className="divSettings halfWidthDiv">
                <Card
                  title="Map settings"
                >
                  <div className="inputContainer">
                    <p>Map's default center:</p>
                    <Checkbox
                      checked={this.state.settings.defaultCenter === null}
                      label="Use user's location as map center"
                      onChange={() => {
                        if (this.state.settings.defaultCenter === null) { // if is being unchecked
                          this.setState({
                            shouldBannerDisplay: true,
                            settings: {
                              ...this.state.settings,
                              defaultCenter: {
                                lat: 40.7127753,
                                lng: -74.0059728,
                              }
                            }
                          })
                        }
                        else { // if is being checked
                          this.setState({
                            shouldBannerDisplay: true,
                            settings: {
                              ...this.state.settings,
                              defaultCenter: null
                            }
                          })
                        }
                      }}
                    />
                    {this.state.settings.defaultCenter !== null &&
                      <Map
                        center={{ lat: this.state.settings.defaultCenter.lat, lng: this.state.settings.defaultCenter.lng }}
                        height='300px'
                        zoom={7}
                        actionOnUpdateLatLng={() => {
                          this.setState({
                            shouldBannerDisplay: true,
                            settings: {
                              ...this.state.settings,
                              defaultCenter: {
                                lat: this.props.currentLocation.lat,
                                lng: this.props.currentLocation.lng
                              }
                            }
                          })
                        }
                        }
                      />
                    }
                    <br /><br /><br />
                    <TextField
                      name="zoomLevel"
                      label="Zoom level:"
                      value={this.state.settings.zoomLevel}
                      onChange={val => this.handleFormInputChange("zoomLevel", val)}
                      type="number"
                    />
                  </div>
                </Card>
              </div>
            </div>

            <div className="divSectionContainer">
              <div className="divSettings">
                <Card
                  title="Language & Content"
                >
                  <div className="inputContainer">
                    {!!this.state.settings.content[this.state.currentLanguage].sectionHeader &&
                      <>
                        <div className="selectContainer">
                          <Select
                            name="language"
                            label="Language:"
                            value={this.state.currentLanguage}
                            onChange={val => this.handleLanguageSettingsChange(val)}
                            options={langList}
                          />
                          <Button
                            onClick={this.handleLanguageAddToggleClick}
                            ariaExpanded={this.state.addingLanguage}
                            ariaControls="basic-collapsible"
                          >
                            + Add language
                          </Button>
                          
                          <Collapsible open={this.state.addingLanguage} id="basic-collapsible">
                              <Select
                                name="addLanguage"
                                label="Choose a language"
                                value={this.state.languageToAdd}
                                onChange={val => this.setState({ languageToAdd: val })}
                                options={languages}
                              />
                              <Button onClick={this.handleAddNewLanguage}>Add</Button>
                          </Collapsible>
                        </div>

                        <TextField
                          name="sectionHeader"
                          label="Header Text: "
                          value={this.state.settings.content[this.state.currentLanguage].sectionHeader}
                          onChange={val => this.handleLanguageContentChange("sectionHeader", val)}
                          maxLength={100}
                        />
                        <TextField
                          name="storeName"
                          label="Store Name Text: "
                          value={this.state.settings.content[this.state.currentLanguage].storeName}
                          onChange={val => this.handleLanguageContentChange("storeName", val)}
                          maxLength={100}
                        />
                        <TextField
                          name="customAddress"
                          label="Address Text: "
                          value={this.state.settings.content[this.state.currentLanguage].customAddress}
                          onChange={val => this.handleLanguageContentChange("customAddress", val)}
                          maxLength={100}
                        />
                        <TextField
                          name="customProvince"
                          label="Province Text: "
                          value={this.state.settings.content[this.state.currentLanguage].customProvince}
                          onChange={val => this.handleLanguageContentChange("customProvince", val)}
                          maxLength={100}
                        />
                        <TextField
                          name="customCountry"
                          label="Country Text: "
                          value={this.state.settings.content[this.state.currentLanguage].customCountry}
                          onChange={val => this.handleLanguageContentChange("customCountry", val)}
                          maxLength={100}
                        />
                        <TextField
                          name="zipCode"
                          label="Zip Code Text: "
                          value={this.state.settings.content[this.state.currentLanguage].zipCode}
                          onChange={val => this.handleLanguageContentChange("zipCode", val)}
                          maxLength={100}
                        />
                        <TextField
                          name="phone"
                          label="Phone Text: "
                          value={this.state.settings.content[this.state.currentLanguage].phone}
                          onChange={val => this.handleLanguageContentChange("phone", val)}
                          maxLength={100}
                        />
                        <TextField
                          name="email"
                          label="Email Text: "
                          value={this.state.settings.content[this.state.currentLanguage].email}
                          onChange={val => this.handleLanguageContentChange("email", val)}
                          maxLength={100}
                        />
                        <TextField
                          name="fax"
                          label="Fax Text: "
                          value={this.state.settings.content[this.state.currentLanguage].fax}
                          onChange={val => this.handleLanguageContentChange("fax", val)}
                          maxLength={100}
                        />
                        <TextField
                          name="website"
                          label="Website Text: "
                          value={this.state.settings.content[this.state.currentLanguage].website}
                          onChange={val => this.handleLanguageContentChange("website", val)}
                          maxLength={100}
                        />
                        <TextField
                          name="note"
                          label="Note Text: "
                          value={this.state.settings.content[this.state.currentLanguage].note}
                          onChange={val => this.handleLanguageContentChange("note", val)}
                          maxLength={100}
                        />
                        <TextField
                          name="tags"
                          label="Tags Text: "
                          value={this.state.settings.content[this.state.currentLanguage].tags}
                          onChange={val => this.handleLanguageContentChange("tags", val)}
                          maxLength={100}
                        />
                      </>
                    }

                  </div>
                </Card>
              </div>
            </div>
            {this.state.shouldBannerDisplay &&
              <Banner title={this.state.bannerTitle} status={this.state.bannerStatus} >
                <p>{this.state.bannerMsg}</p>
              </Banner>
            }
            <Card>
              <div className="settingsWrapper">
                <Button
                  primary
                  onClick={this.saveAllChanges}
                >
                  Save all changes
                </Button>
              </div>
            </Card>
          </>
        }
      </>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Settings);