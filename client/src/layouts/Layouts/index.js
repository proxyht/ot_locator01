import React, { Component } from 'react';
import './style.sass';
import Axios from 'axios';
import { connect } from 'react-redux';
// import { updateGoogleAPIKey, updateQueryString } from '../../actions';
import { Button, Heading, Card, TextField, Banner, Loading, ChoiceList, Thumbnail } from '@shopify/polaris';
import { _helper_api_makeParams } from '../../helper/api';
import Style1Img from './assets/style-1.png';
import Style2Img from './assets/style-2.png';
import Style3Img from './assets/style-3.png';

const mapStateToProps = state => {
  return {
    queryString: state.AppAction.queryString,
  };
};

function mapDispatchToProps(dispatch) {
  return {
  };
}

class Layouts extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dialogType: this.props.dialogType,
      shouldBannerDisplay: false,
      bannerTitle: 'No changes have been made',
      bannerMsg: 'Remember to save your changes',
      bannerStatus: 'warning',
      layoutSettings: {
        layout: 'style-1',
        paddingSlides: null,
        locationListWidth: null,
        sectionHeader: null,
        mapWidth: null,
        mapAndListHeight: null,
        responsiveBreakpoint: null,
      },
      isLoading: true,
    }
  }

  handleInputChange = (key, val) => {
    this.setState({
      shouldBannerDisplay: true,      
      layoutSettings: {
        shouldBannerDisplay: true,
        ...this.state.layoutSettings,
        [key]: val
      }
    })
  }

  saveAllChanges = (e) => {
    e.preventDefault();

    let paramsObj = _helper_api_makeParams(this.props.queryString, this.props.currentLocation);
    paramsObj['layoutSettings'] = this.state.layoutSettings;
    console.log(paramsObj);

    // fire request to server
    Axios
      .post('/api/settings/layout/change', paramsObj)
      .then((response) => {
        if (response.data.status) {
          this.setState({
            bannerTitle: "Layout updated",
            bannerStatus: 'success',
            bannerMsg: "All changes has been saved"
          });
          // setTimeout(() => window.location.reload(), 500);
        }
        else {
          this.setState({
            bannerTitle: "Server error",
            bannerStatus: 'critical',
            bannerMsg: "No changes have been made"
          });
        }
      })
  }

  async componentDidMount() {
    // get settings data from server
    console.log(this.props);

    let response = await Axios.get(`/api/settings/layout/get${this.props.queryString}`)

    if (response !== null) {
      this.setState({
        layoutSettings: response.data,
        isLoading: false,
      })
    }
  }

  render() {

    const layoutChoices = [
      {
        label:
          <Thumbnail
            source={Style1Img}
            alt="style-1: Default"
            size="large"
          />,
        value: 'style-1'
      },
      {
        label:
          <Thumbnail
            source={Style2Img}
            alt="style-2: Left"
            size="large"
          />,
        value: 'style-2'
      },
      {
        label:
          <Thumbnail
            source={Style3Img}
            alt="style-3: Right"
            size="large"
          />,
        value: 'style-3'
      },
    ];
    return (
      <>

        {this.state.isLoading &&
          <Loading />
        }
        {!this.state.isLoading &&
          <>
            {this.state.shouldBannerDisplay &&
              <Banner title={this.state.bannerTitle} status={this.state.bannerStatus} >
                <p>{this.state.bannerMsg}</p>
              </Banner>
            }
            <Card>
              <div className="settingsWrapper">
                <Heading>
                  Layouts settings
              </Heading>
                <div className="inputContainer">
                  <ChoiceList
                    title={"Select layout"}
                    choices={layoutChoices}
                    selected={[this.state.layoutSettings.layout]}
                    onChange={(val) => this.handleInputChange("layout", val[0])}
                  />
                </div>
                <div className="inputContainer">
                  <TextField
                    label="Padding Slides:"
                    name="paddingSlides"
                    helpText="The padding (in px) that will be applied to the left and right sides of the container. Default = 0px"
                    value={this.state.layoutSettings.paddingSlides}
                    onChange={(val) => this.handleInputChange("paddingSlides", val)}
                  />
                  <TextField
                    label="Location list width:"
                    name="locationListWidth"
                    helpText="The width in % of the location list (Left and Right layouts only). Default = 35%"
                    value={this.state.layoutSettings.locationListWidth}
                    onChange={(val) => this.handleInputChange("locationListWidth", val)}
                  />
                  <TextField
                    label="Map width:"
                    name="mapWidth"
                    helpText="The width in % of the map (Left and Right layouts only). Default = 65%"
                    value={this.state.layoutSettings.mapWidth}
                    onChange={(val) => this.handleInputChange("mapWidth", val)}
                  />

                  <TextField
                    label="Map & List height:"
                    name="mapAndListHeight"
                    helpText="The height of the map and location list in px (Left and Right layouts only). Default = 400px"
                    value={this.state.layoutSettings.mapAndListHeight}
                    onChange={(val) => this.handleInputChange("mapAndListHeight", val)}
                  />
                  <TextField
                    label="Responsive breakpoint:"
                    name="responsiveBreakpoint"
                    helpText="The max width of the screen in px where the map and location list will become stack for mobile devices. Default = 960px"
                    value={this.state.layoutSettings.responsiveBreakpoint}
                    onChange={(val) => this.handleInputChange("responsiveBreakpoint", val)}
                  />
                </div>
              </div>
            </Card>
            {this.state.shouldBannerDisplay &&
              <Banner title={this.state.bannerTitle} status={this.state.bannerStatus} >
                <p>{this.state.bannerMsg}</p>
              </Banner>
            }
            <Card>
              <div className="settingsWrapper">
                <Button
                  primary
                  onClick={this.saveAllChanges}
                >
                  Save all changes
              </Button>
              </div>
            </Card>
          </>
        }
      </>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Layouts);