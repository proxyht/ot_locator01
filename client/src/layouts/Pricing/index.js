import React, { Component } from 'react';
import './style.sass';
import Axios from 'axios';
import { connect } from 'react-redux';
import { List, Heading, Card, Button, Banner, Spinner, Subheading } from '@shopify/polaris';
import { _helper_api_setAuth, } from '../../helper/api';

const mapStateToProps = state => {
    return {
        queryString: state.AppAction.queryString,
        googleAPIKey: state.GoogleAction.googleAPIKey,
    };
};

function mapDispatchToProps(dispatch) {
    return {
    };
}

class Pricing extends Component {
    planItem = (elementKey, planName, heading, subheading, contents, isButtonActive) => {
       
        return (
                <div key={elementKey} className={"planItemContainer "+(planName === this.state.activePlan ? "active":"")}>
                    <Heading>{heading}</Heading><br/>
                    <Subheading>{subheading}</Subheading><br/><br/>
                    <List type="bullet">
                    {contents.map((v,k) => {
                        return (<List.Item key={k}>{v}</List.Item>)
                    })}
                    </List>
                    <div className="purchaseButton">
                        <Button 
                        disabled={!isButtonActive}
                        onClick={() => this.handleChangePlan(planName)}
                        >
                            {(planName === this.state.activePlan ? "Active":"Choose this plan")}
                        </Button>
                    </div>
                </div>
        );
    }

    constructor(props) {
        super(props);

        console.log(this.props);
        this.state = {
            activePlan: null,
        }
    }

    async componentDidMount() {
        
        let currentPlan = (await Axios.get(`/api/pricing/getCurrentPlan${this.props.queryString}`)).data;
        console.log(currentPlan);
        this.setState({
            activePlan: currentPlan, // 'free' for example
        });
    }

    handleChangePlan = async (planName) => {
        let paramsObj = _helper_api_setAuth(this.props.queryString);
        paramsObj['plan'] = planName;

        let response = (await Axios.post('/api/pricing/upgrade',paramsObj)).data;
        if (response.status){
            let confirmationLink = response.data;

            window.open(`/upgrade/confirmation?link=${confirmationLink}`);
        } else{
            alert("Server error. Cannot change plan");
        }
    }

    render() {
        const planList = [
            {
                name: 'free',
                priority: 0,
                price: 'free',
                heading: 'FREE',
                contents: [
                    '10 locations',
                    '24 hours support'
                ],
            },
            {
                name: 'premium',
                priority: 1,
                price: '$10.00/month',
                heading: 'PREMIUM',
                contents: [
                    'Unlimited locations',
                    'Advance support',
                    'First to test new features'
                ],
            },
        ];
        const planPriority = {
            'free': 0, // lowest
            'premium': 1,
        };

        return (
            <>
                {this.state.activePlan &&
                    <Card>
                        {planList.map((v,k) => {
                            return this.planItem(k, v.name, v.heading, v.price, v.contents, v.priority > planPriority[this.state.activePlan])
                        })}
                    </Card>
                }
            </>
        )

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Pricing);