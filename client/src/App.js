import React, { Component } from 'react';
import './App.css';
import Axios from 'axios';
import { AppProvider, Tabs, Card, Frame } from '@shopify/polaris';
import IndexPage from './layouts/IndexPage';
import Settings from './layouts/Settings';
import HowToUse from './layouts/HowToUse';
import ImportExport from './layouts/ImportExport';
import Layouts from './layouts/Layouts';
import Pricing from './layouts/Pricing';

import { connect } from 'react-redux';
import { updateGoogleAPIKey, updateQueryString } from './actions';

const tabs = [
	{
		id: 'locations',
		content: 'Locations',
		accessibilityLabel: 'All customers'
	},
	{
		id: 'settings',
		content: 'Settings'
	},
	{
		id: 'importexport',
		content: 'Import & Export'
	},
	{
		id: 'layouts',
		content: 'Layouts'
	},
	{
		id: 'pricing',
		content: 'Pricing'
	},
	{
		id: 'guidelines',
		content: 'How to use'
	}
];

const tabPanels = [
	<Tabs.Panel id="locations">
		<IndexPage />
	</Tabs.Panel>,
	<Tabs.Panel id="settings">
		<Settings />
	</Tabs.Panel>,
	<Tabs.Panel id="importexport">
		<ImportExport />
	</Tabs.Panel>,
	<Tabs.Panel id="layouts">
		<Layouts />
	</Tabs.Panel>,
	<Tabs.Panel id="Pricing">
		<Pricing />
	</Tabs.Panel>,
	<Tabs.Panel id="guidelines">
		<HowToUse />
	</Tabs.Panel>
];

const mapStateToProps = (state) => {
	return {
		googleAPIKey: state.GoogleAction.googleAPIKey,
		queryString: state.AppAction.queryString
	};
};

function mapDispatchToProps(dispatch) {
	return {
		updateGoogleAPIKey: (apiKey) => dispatch(updateGoogleAPIKey(apiKey)),
		updateQueryString: (queryString) => dispatch(updateQueryString(queryString))
	};
}

class App extends Component {
	constructor(props) {
		super(props);

		this.state = {
			selectedTab: 0
		};
	}

	handleTabChange = (tabIndex) => {
		this.setState({
			selectedTab: tabIndex
		});
	};
	
	async componentWillMount() {
		// update url params to global redux state
		await this.props.updateQueryString( window.location.search);
		console.log(this.props)
		// retrieve shop and token from url params
		let queryString = this.props.queryString.substr(1);
		let splittedParams = queryString.split('&');

		let paramsObj = {};
		for (let index in splittedParams) {
			let param = splittedParams[index].split('=');
			paramsObj[param[0]] = param[1];
		}
		console.log('Requesting for google api key');
		let requestForAPIKey = await Axios.get(`/api/settings/get${this.props.queryString}`);

		let apiKey = requestForAPIKey.data.googleAPIKey;
		console.log(apiKey);
		this.props.updateGoogleAPIKey(apiKey);
	}
	
	componentDidMount(){
		
		// nếu user đã hết hạn plan => chuyển sang trang pricing
		if (window.location.search.includes('isOverdue=1')){
			this.handleTabChange(4);
		}
	}

	render() {
		const selected = this.state.selectedTab;

		return (
			<div className="App">
				
				<AppProvider>
					<Frame>
						<Tabs tabs={tabs} selected={selected} onSelect={this.handleTabChange}>
							<Card.Section>{tabPanels[selected]}</Card.Section>
						</Tabs>
					</Frame>
				</AppProvider>
			</div>
		);
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
