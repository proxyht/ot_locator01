const express = require('express');
const path = require('path');
const axios = require('axios');

const app = express();
const PORT = process.env.port || 3000;

const CONST = require('./constants');
const FUNC = require('./modules/customFunc');
const db = require('./modules/database');

app.use(express.json());
app.use(express.static('build'));
app.use(express.static('test'));

app.use('/api',require('./routes/api'));
app.use('/storefront',require('./routes/storefront'));
app.use('/auth',require('./routes/auth'));

app.listen(PORT, () => {
    console.log("SERVER UP AND RUNNING ON PORT " + PORT);
})

app.get('/upgrade/confirmation',(req,res) =>{
    console.log("RE")
    res.redirect(req.query.link);
})

// when app is uninstalled, shopify will fire into this route
app.post('/uninstall', (req, res) => {
    console.log(req.body);
    console.log(`APP UNINSTALLED BY ${req.body.domain}`);

    // db.deleteUser({
    //     shop: req.body.domain
    // });

    db.deactivateUser(req.body.domain)

    res.status(200).send("OK");
})



app.get('/test', async (req, res) => {
    // let fs = require('fs');
    // fs.unlinkSync(path.join(__dirname,'/uploads/test'))
    // res.status(200).send("ok");
    console.log(req);
    res.sendStatus(200);
});

app.get('/node/test',(req,res)=>{
    res.sendStatus(200);
})

app.get('/testconn', (req,res) => {
    
})